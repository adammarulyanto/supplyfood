<?php
defined('BASEPATH') or exit ('no direct script access allowed');

class main_model extends CI_Model{
	function edit_data($where,$table){
		return $this->db->get_where($table,$where);
	}
	function get_data($table){
		return $this->db->get($table);
	}
	function insert_data($data,$table){
		$this->db->insert($table,$data);
	}
	function update_data($where,$data,$table){
		$this->db->where($where);
		$this->db->update($table,$data);
	}
	function delete_data($where,$table){
		$this->db->where($where);
		$this->db->delete($table);
	}
	function find($where,$table){
		$hasil = $this->db->where('id_produk',$where)
			-> limit(1)
			-> get($table);
		if($hasil->num_rows()>0){
			return $hasil->row();
		}else{
			return 0;
		}
	}
	function thousandsCurrencyFormat($num) {

    if( $num > 1000 ) {

        $x = round($num);
        $x_number_format = number_format($x);
        $x_array = explode(',', $x_number_format);
        $x_parts = array('k', 'm', 'b', 't');
        $x_count_parts = count($x_array) - 1;
        $x_display = $x;
        $x_display = $x_array[0] . ((int) $x_array[1][0] !== 0 ? '.' . $x_array[1][0] : '');
        $x_display .= $x_parts[$x_count_parts - 1];
        
        return $x_display;
    }

    return $num;
}
}
?>