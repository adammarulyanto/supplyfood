<style type="text/css">
body{
background: -moz-linear-gradient(91deg, rgba(33,73,122,1) 0%, rgba(0,128,128,1) 100%); /* ff3.6+ */
background: -webkit-gradient(linear, left top, left bottom, color-stop(0%, rgba(0,128,128,1)), color-stop(100%, rgba(33,73,122,1))); /* safari4+,chrome */
background: -webkit-linear-gradient(91deg, rgba(33,73,122,1) 0%, rgba(0,128,128,1) 100%); /* safari5.1+,chrome10+ */
background: -o-linear-gradient(91deg, rgba(33,73,122,1) 0%, rgba(0,128,128,1) 100%); /* opera 11.10+ */
background: -ms-linear-gradient(91deg, rgba(33,73,122,1) 0%, rgba(0,128,128,1) 100%); /* ie10+ */
background: linear-gradient(359deg, rgba(33,73,122,1) 0%, rgba(0,128,128,1) 100%); /* w3c */
filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#008080', endColorstr='#21497A',GradientType=0 ); /* ie6-9 */
}
</style>

<?php 
if($bayar->status=='2'){
  redirect(base_url().'login');
}
?>

<!-- no additional media querie or css is required -->
  <div class="container">
      <div class="row justify-content-center align-items-center" style="height:100vh">
          <div class="col-sm-12 col-lg-7">
                  <div class="card-body">
                    <div class="head-form">
                      <h1>Terima Kasih</h1>
                      <p>Pelayan akan datang mengantar kembalian anda di meja.</p>
                    </div>
                      <div class="bill">
                        <div class="card-body-pembayaran">
                    <div class="head-form">
                      <a href="<?=base_url()?>kasir"><h1><?=$bayar->kd_pesanan?></h1></a>
                      <p><?=$bayar->nm_pemesan?></p>
                      <p class="tgl_bayar"><?=$bayar->tgl_bayar?></p>
                    </div>
                      <div class="content-dashboard-kasir">
                          <div class="list-bayar-area">
                          <?php
                                      foreach ($list_bayar as $list_bayar) {
                                    ?>
                                      <hr>
                                      <div class="list-bar-menu-det-bayar" onClick="document.location.href='#'">
                                      <h5><?=$list_bayar->nm_produk?></h5>
                                      <p>Quantity : <?=$list_bayar->qty?></p>
                                      <p class="harga-bayar">Rp. <?=$list_bayar->harga?></p>
                                      </div>
                                      <hr>
                                    <?php
                                    }
                                    ?>
                                  </div>
                                  <hr>
                                  <hr>
                                  <div class="list-bar-menu-det-bayar" onClick="document.location.href='#'">
                                    <h5>Nominal Bayar</h5>
                                    <p class="total-bayar">Rp. <?=number_format($bayar->nominal_bayar)?></p>
                                  </div>
                                  <div class="list-bar-menu-det-bayar" onClick="document.location.href='#'">
                                    <h5>Total Tagihan</h5>
                                    <p class="total-bayar">Rp. <?=number_format($bayar->total_tagihan)?></p>
                                  </div>
                                  <div class="list-bar-menu-det-bayar" onClick="document.location.href='#'">
                                    <h5>Kembali</h5>
                                    <p class="total-bayar">Rp. <?=number_format($bayar->kembali)?></p>
                                  </div><br>
                                  <form method="post" action="<?=base_url()?>kasir/pesanan_lunas/<?=$bayar->id_pesanan?>">
                                    </form>
                      </div>
                    </div>
                      </div>
                    </div>
            </div>
        </div>
    </div>