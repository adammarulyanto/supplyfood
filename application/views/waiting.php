<?php
$page = base_url().'meja/waiting?kd_pesanan='.$_GET['kd_pesanan'];
$sec = "15";
?>



<meta http-equiv="refresh" content="<?php echo $sec?>;URL='<?php echo $page?>'">
<style type="text/css">
body{
background: -moz-linear-gradient(91deg, rgba(33,73,122,1) 0%, rgba(0,128,128,1) 100%); /* ff3.6+ */
background: -webkit-gradient(linear, left top, left bottom, color-stop(0%, rgba(0,128,128,1)), color-stop(100%, rgba(33,73,122,1))); /* safari4+,chrome */
background: -webkit-linear-gradient(91deg, rgba(33,73,122,1) 0%, rgba(0,128,128,1) 100%); /* safari5.1+,chrome10+ */
background: -o-linear-gradient(91deg, rgba(33,73,122,1) 0%, rgba(0,128,128,1) 100%); /* opera 11.10+ */
background: -ms-linear-gradient(91deg, rgba(33,73,122,1) 0%, rgba(0,128,128,1) 100%); /* ie10+ */
background: linear-gradient(359deg, rgba(33,73,122,1) 0%, rgba(0,128,128,1) 100%); /* w3c */
filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#008080', endColorstr='#21497A',GradientType=0 ); /* ie6-9 */
}
</style>
<?php
if(isset($_GET['warn'])){
    if($_GET['warn']=='desc'){
    $page = base_url().'meja/waiting?kd_pesanan='.$_GET['kd_pesanan'];
?>
<div class="modal modal-popup">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" onClick="location.href='<?=$page?>'"><span>×</span></button>
                     </div>
          
                    <div class="modal-body">
                       
            <div class="thank-you-pop-desc">
              <h2 align="left">Status Pesanan</h2>
              <table cellpadding="0" cellspacing="0" border="0" width="100%" class="table-desc">
                <tr>
                <td><span class="btn btn-primary" onClick="location.href='<?=base_url()?>meja/waiting?kd_pesanan=<?=$_GET['kd_pesanan']?>&warn=desc'">Pengecekan</span></td>
                <td>Pesanan sedang dalam antrian untuk di cek oleh kasir, selama masih status ini, pesanan dapat di ubah atau di batalkan</td>
                </tr>
                <tr>
                <td><span class="btn btn-primary" onClick="location.href='<?=base_url()?>meja/waiting?kd_pesanan=<?=$_GET['kd_pesanan']?>&warn=desc'">Dalam List</span></td>
                <td>Pesanan sudah dalam list proses, di status ini pesanan tidak bisa di batalkan</td>
                </tr>
                <tr>
                <td><span class="btn btn-warning" onClick="location.href='<?=base_url()?>meja/waiting?kd_pesanan=<?=$_GET['kd_pesanan']?>&warn=desc'">Dimasak</span></td>
                <td>Pesanan sedang di proses oleh bagian dapur</td>
                </tr>
                <tr>
                <td><span class="btn btn-warning" onClick="location.href='<?=base_url()?>meja/waiting?kd_pesanan=<?=$_GET['kd_pesanan']?>&warn=desc'">Finishing</span></td>
                <td>Pesanan sedang dalam penyelesaiian sebelum di antarkan</td>
                </tr>
                <tr>
                <td><span class="btn btn-success" onClick="location.href='<?=base_url()?>meja/waiting?kd_pesanan=<?=$_GET['kd_pesanan']?>&warn=desc'">Selesai</span></td>
                <td>di status ini Pesanan seharusnya sedang di antarkan ataupun pesanan sudah tiba di meja</td>
                </tr>
              </table>
            </div>
                         
                    </div>
          
                </div>
            </div>
        </div>
<?php
}
}
?>
<!-- no additional media querie or css is required -->
  <div class="container">
      <div class="row justify-content-center align-items-center" style="height:100vh">
          <div class="col-sm-12 col-lg-6">
                  <div class="card-body">
                    <h1>Status Pesanan</h1>
                    <p>Pesanan sedang di proses</p>
                    <div class="progress" style="height: 30px;">
                      <div class="progress-bar" style="width:<?=$summary->persen_selesai?>%; background-color: #2ecc71;"></div>
                    </div>
                      <div class="box-list">
                      <?php
                        $no=0;
                         foreach ($det_pesanan as $det_pesanan) {
                        $no++;
                      ?>
                        <div class="list-bar-menu-waiting" onClick="document.location.href='#'">
                          <ul class="main-menu-bar-menu">
                            <li><?=$det_pesanan->nm_produk?></li>
                            <li>
                              <?php
                              if($det_pesanan->status==0){
                              ?>
                                <span class="btn btn-primary flag-pesan" onClick="location.href='<?=base_url()?>meja/waiting?kd_pesanan=<?=$_GET['kd_pesanan']?>&warn=desc'">Pengecekan</span>
                              <?php
                              }else if($det_pesanan->status==1){
                              ?>
                                <span class="btn btn-primary flag-pesan" onClick="location.href='<?=base_url()?>meja/waiting?kd_pesanan=<?=$_GET['kd_pesanan']?>&warn=desc'">Dalam List</span>
                              <?php
                              }else if($det_pesanan->status==2){
                              ?>
                                <span class="btn btn-warning flag-pesan" onClick="location.href='<?=base_url()?>meja/waiting?kd_pesanan=<?=$_GET['kd_pesanan']?>&warn=desc'">Dimasak</span>
                              <?php
                              }else if($det_pesanan->status==3){
                              ?>
                                <span class="btn btn-warning flag-pesan" onClick="location.href='<?=base_url()?>meja/waiting?kd_pesanan=<?=$_GET['kd_pesanan']?>&warn=desc'">Finishing</span>
                              <?php
                              }else{
                              ?>
                                <span class="btn btn-success flag-pesan" onClick="location.href='<?=base_url()?>meja/waiting?kd_pesanan=<?=$_GET['kd_pesanan']?>&warn=desc'">Selesai</span>
                              <?php
                              }
                              ?>
                            </li>
                          </ul>
                        </div>
                      <?php
                      }
                      ?>
                      </div>
                      <ul class="box-btn-waiting">
                        <li><button type="submit" id="sendlogin" class="btn btn-primary btn-pembayaran1" onClick="document.location.href='<?=base_url()?>meja/menu/1?kd_pesanan=<?=$_GET["kd_pesanan"]?>'"><i class="fa fa-chevron-left"></i> &nbsp;&nbsp;Menu</button></li>
                        <?php
                        if($summary->persen_selesai==100){
                        ?>
                        <li><button type="submit" id="sendlogin" class="btn btn-pembayaran1 btn-green" onClick="location.href='<?=base_url()?>meja/det_pembayaran/<?=$summary->id_pesanan?>'" >Bayar</button></li>
                        <?php
                        }else{
                        ?>
                        <li><button type="submit" id="sendlogin" class="btn btn-pembayaran1 disable" disabled="">Bayar</button></li>
                        <?php
                        }
                        ?>
                      </ul>
                    </div>
            </div>
        </div>
    </div>
