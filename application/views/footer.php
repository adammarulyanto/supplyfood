
    <!-- /#page-content-wrapper -->
  </div>
  <!-- /#wrapper -->
  <script type="text/javascript">
   $(function active_menu() {                       //run when the DOM is ready
    $("a.sidemenu-list-child").click(function() {  //use a class, since your ID gets mangled
      $("a.sidemenu-list-child li").addClass("active");      //add the class to the clicked element
    });
  });
  </script>
  <!-- Bootstrap core JavaScript -->
  <script src="<?=base_url()?>/assets/vendor/jquery/jquery.min.js"></script>
  <script src="<?=base_url()?>/assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <!-- Menu Toggle Script -->
  <script>
    $("#menu-toggle").click(function(e) {
      e.preventDefault();
      $("#wrapper").toggleClass("toggled");
    });
  </script>

</body>

</html>
