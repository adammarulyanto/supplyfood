<div class="container-dashboard">
  <div class="head-menu">
    <h4 align="center"><?=$pesanan->nm_pemesan?></h4>
    <p align="center">Meja</p>
    <h1 align="center"><?=$idmeja->kd_meja?></h1>
    <h5 align="center" onClick="document.location.href='<?=base_url()?>meja/keranjang?kd_pesanan=<?=$_GET["kd_pesanan"]?>'"><span class="label-jml-pesanan"><?=$pesanan->jml?> Pesanan &nbsp;&nbsp;&nbsp;<i class="fa fa-chevron-right"></i></span></h5>
  </div>
  <div class="content-menu">
    <div class="main-bar">
      <?php
      if($this->uri->segment(3)==1){
      ?>
      <div class="col-main-bar">
          <button  onClick="document.location.href='#'" class="active">Makanan</button>
      </div>
      <div class="col-main-bar">
          <button  onClick="document.location.href='<?=base_url()?>meja/menu/2?kd_pesanan=<?=$_GET["kd_pesanan"]?>'">Minuman</button>
      </div>
      <?php
      }else{
      ?>
      <div class="col-main-bar">
          <button  onClick="document.location.href='<?=base_url()?>meja/menu/1?kd_pesanan=<?=$_GET["kd_pesanan"]?>'">Makanan</button>
      </div>
      <div class="col-main-bar">
          <button  onClick="document.location.href='#'" class="active">Minuman</button>
      </div>
      <?php
      }
      ?>
    </div>
    <?php
    if($this->uri->segment(3)==1){
        foreach ($produk as $produk) {
        if($produk->id_jenis_produk==1){
        ?>
        <div class="list-bar-menu" onClick="document.location.href='#'">
          <div class="box-img" style="background: url(<?=base_url()?>assets/images/produk/makanan/<?=$produk->gambar?>); background-size: cover; background-position: center;"></div>
          <p class="title-menu"><?=$produk->nm_produk?></p>
              <span class="desc_produk"><?=$produk->desc_produk?></span>
          <hr>
          <ul class="main-menu-bar-menu">
            <li style="text-align: left;">Rp. <?=$this->main_model->thousandsCurrencyFormat($produk->harga)?></li>
            <li><p> <?=anchor('meja/tambah_keranjang/'.$produk->id_produk.'?kd_pesanan='.$_GET['kd_pesanan'],'+ Add',
                [
                  'class' => 'btn btn-primary btn-add-pesan',
                  'role'  => 'button'
                ])?>
                </p></li>
          </ul>
        </div>
        <?php
        }
        }
    }else if ($this->uri->segment(3)==2){
        foreach ($produk as $produk) {
        if($produk->id_jenis_produk==2){
        ?>
        <div class="list-bar-menu" onClick="document.location.href='#'">
          <div class="box-img" style="background: url(<?=base_url()?>assets/images/produk/makanan/<?=$produk->gambar?>); background-size: cover; "></div>
          <p class="title-menu"><?=$produk->nm_produk?></p>
          <span class="desc_produk"><?=$produk->desc_produk?></span>
          <hr>
          <ul class="main-menu-bar-menu">
            <li style="text-align: left;">Rp. <?=$this->main_model->thousandsCurrencyFormat($produk->harga)?></li>
            <li><p> <?=anchor('meja/tambah_keranjang/'.$produk->id_produk.'?kd_pesanan='.$_GET['kd_pesanan'],'+ Add',
                [
                  'class' => 'btn btn-primary btn-add-pesan',
                  'role'  => 'button'
                ])?>
                </p></li>
          </ul>
        </div>
        <?php
        }
        }
    }
        ?>
    