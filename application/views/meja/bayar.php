<!-- no additional media querie or css is required -->
  <div class="container">
      <div class="row justify-content-center align-items-center" style="height:100vh">
          <div class="col-sm-12 col-lg-5">
                  <div class="card-body-pembayaran">
                    <div class="head-form">
                      <a href="<?=base_url()?>kasir"><h1><?=$bayar->kd_pesanan?></h1></a>
                      <p><?=$bayar->nm_pemesan?></p>
                    </div>
				    <div class="content-dashboard-kasir">
				    		<div class="list-bayar-area">
				    		<?php
                        		foreach ($list_bayar as $list_bayar) {
	                      	?>
		                        <hr>
		                        <div class="list-bar-menu-det-bayar" onClick="document.location.href='#'">
		                        <h5><?=$list_bayar->nm_produk?></h5>
		                        <p>Quantity : <?=$list_bayar->qty?></p>
		                        <p class="harga-bayar">Rp. <?=$list_bayar->harga?></p>
		                        </div>
		                        <hr>
	                      	<?php
	                      	}
	                      	?>
	                  		</div>
	                      <hr>
	                      <div class="list-bar-menu-det-bayar" onClick="document.location.href='#'">
	                        <h5>Total</h5>
	                        <p class="total-bayar">Rp. <?=number_format($bayar->total_bayar)?></p>
	                      </div>
	                      <hr>
	                      <div class="list-bar-menu-det-bayar" onClick="document.location.href='#'">
	                        <h5>Sudah di Bayarkan</h5>
	                        <p class="total-bayar">Rp. <?=$bayar->sdh_terbayar?></p>
	                      </div>
	                      <div class="list-bar-menu-det-bayar" onClick="document.location.href='#'">
	                        <h5>Belum di Bayarkan</h5>
	                        <p class="total-bayar">Rp. <?=$bayar->blm_terbayar?></p>
	                      </div><br>
					    	<form action="<?=base_url()?>meja/bayar" method="post">
					    	<div class="nominal-bar">
					    		<b>Rp</b>
					    			<input type="text" name="nominal" class="form-control form-nominal-bayar" autocomplete="off" placeholder="Masukan Nominal Pembayaran" required="">
					    			<input type="hidden" name="total_bayar" value="<?=$bayar->blm_terbayar_norm?>">
					    			<input type="hidden" name="id_pesanan" value="<?=$bayar->id_pesanan?>">
					    			<input type="hidden" name="kd_pesanan" value="<?=$bayar->kd_pesanan?>">
					    	</div>
					    	<br>
                          	<button type="submit" id="sendlogin" class="btn btn-primary btn-bayar">Bayar</button>
					    	</form>
				    </div>
                    </div>
            </div>
        </div>
    </div>