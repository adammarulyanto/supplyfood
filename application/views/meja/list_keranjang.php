<div class="kepala_keranjang">
          <br>
          <ul class="summary_keranjang">
            <li>
            <span>Harga (Rp)</span>
            <h1><?=$total_harga->total_harga?></h1>
            </li>
            <li>
              <a href="<?=base_url()?>meja/det_pembayaran/<?=$total_harga->id_pesanan?>" class="btn btn-success btn-next">Selesai <i class="fa fa-angle-right"></i></a>
              <!-- <a href="<?=base_url().'meja/waiting?&kd_pesanan='.$_GET['kd_pesanan']?>" class="btn btn-success btn-next">Next <i class="fa fa-angle-right"></i></a> -->
            </li>
          </ul>
        </div>
<?php
              $no=0;
              foreach ($det_pesanan as $det_pesanan) {
              $no++;
            ?>
            <div class="list-bar-menu" onClick="document.location.href='#'">
            <div class="box-img" style="background: url(<?=base_url()?>assets/images/produk/makanan/<?=$det_pesanan->gambar?>); background-size: cover; "></div>
	          <p><?=$det_pesanan->nm_produk?></p>
	          <span class="desc_produk"><?=$det_pesanan->desc_produk?></span>
	          <hr>
	          <ul class="main-menu-bar-menu">
	            <li style="text-align: left;">Rp. <?=$this->main_model->thousandsCurrencyFormat($det_pesanan->total_harga)?></li>
              <li><span class="jml_pesan" ><?=$det_pesanan->qty?></span></li>
              <?php
              if($det_pesanan->status_pembayaran==0){
              ?>
	            <li><p> <?=anchor('meja/tambah_keranjang/'.$det_pesanan->id_produk.'?kd_pesanan='.$_GET['kd_pesanan'],'+',
                  [
                    'class' => 'btn btn-primary btn-add-pesan',
                    'role'  => 'button'
                  ])?>
                  </p></li>
	            <li><p> <?=anchor('meja/kurang_keranjang/'.$det_pesanan->id_produk.'?kd_pesanan='.$_GET['kd_pesanan'],'-',
	                [
	                  'class' => 'btn btn-primary btn-add-pesan kurang-pesan',
	                  'role'  => 'button'
	                ])?>
	                </p></li>
              <?php
              }else{
              ?>
              <li><button class='btn btn-primary btn-add-pesan'>
              <?php
                              if($det_pesanan->status==0){
                              ?>
                                Pengecekan
                              <?php
                              }else if($det_pesanan->status==1){
                              ?>
                                Dalam List
                              <?php
                              }else if($det_pesanan->status==2){
                              ?>
                                Dimasak
                              <?php
                              }else if($det_pesanan->status==3){
                              ?>
                                Finishing
                              <?php
                              }else{
                              ?>
                                Selesai
                              <?php
                              }
                              ?>
                            </button></li>
              <?php
              }
              ?>
	          </ul>
	        </div>
            <?php
            }
            ?>