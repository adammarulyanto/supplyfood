      <div class="container-fluid">
        <div class="head">
          <ul class="head-logo">
            <li class="head-left"><span class="kotak-meja kotak-meja-menu">No Meja : A42</span></li>
            <li class="head-right">
              <div class="chart">
                <h1>3</h1>
                <h5>Pesanan</h5>
              </div>
            </li>
          </ul>
        </div>
        <div class="content-menu">
          <h1>Adam Marulyanto</h1>
        </div>
        <div class="button-next">
            <i class="fas fa-chevron-right"></i>
        </div>
        <?php
        if($this->uri->segment(3)==1){?>
          <div class="menu-kategori">
            <ul class="main-menu-kategori">
              <li class="aktif">Makanan</li>
              <a href="<?=base_url()?>meja/menu/2"><li>Minuman</li></a>
            </ul>
          </div>
          <div class="row grup-list-produk">
            <div class="col-md-2">
              <div class="kotak-produk">
              <img src="<?=base_url()?>/assets/images/produk/makanan/1.png">
                <span class="nama-produk">Ayam Bakar</span>
                <span class="harga-produk">Rp. 25.000.000</span>
                <button type="submit" class="btn btn-primary btn-tambah-keranjang">+ Keranjang</button>
              </div>
            </div>
            <div class="col-md-2">
              <div class="kotak-produk">
              <img src="<?=base_url()?>/assets/images/produk/makanan/1.png">
                <span class="nama-produk">Ayam Bakar</span>
                <span class="harga-produk">Rp. 25.000.000</span>
                <button type="submit" class="btn btn-primary btn-tambah-keranjang">+ Keranjang</button>
              </div>
            </div>
          </div>
        <?php
        }else if ($this->uri->segment(3)==2){?>
          <div class="menu-kategori">
            <ul class="main-menu-kategori">
              <a href="<?=base_url()?>meja/menu/1"><li>Makanan</li></a>
              <li class="aktif">Minuman</li>
            </ul>
          </div>
        <?php
        }
        ?>
      </div>
    