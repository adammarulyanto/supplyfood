<style type="text/css">
	body{
		background: -moz-linear-gradient(45deg, rgba(26,6,6,1) 0%, rgba(64,4,64,1) 100%); /* ff3.6+ */
		background: -webkit-gradient(linear, left bottom, right top, color-stop(0%, rgba(26,6,6,1)), color-stop(100%, rgba(64,4,64,1))); /* safari4+,chrome */
		background: -webkit-linear-gradient(45deg, rgba(26,6,6,1) 0%, rgba(64,4,64,1) 100%); /* safari5.1+,chrome10+ */
		background: -o-linear-gradient(45deg, rgba(26,6,6,1) 0%, rgba(64,4,64,1) 100%); /* opera 11.10+ */
		background: -ms-linear-gradient(45deg, rgba(26,6,6,1) 0%, rgba(64,4,64,1) 100%); /* ie10+ */
		background: linear-gradient(45deg, rgba(26,6,6,1) 0%, rgba(64,4,64,1) 100%); /* w3c */
		filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#400440', endColorstr='#1A0606',GradientType=1 ); /* ie6-9 */
		height: 100%;
		overflow: hidden;
	}
</style>
	<div class="row row-dapur" style="height:100vh">
		<div class="container-dapur">
			<div class="header-dapur">
				<ul class="col-header-dapur">
					<li><p align="left"><?=$this->session->userdata('nm')?></p></li>
					<li><p align="center"><?=$jam->jam?></p></li>
					<li><a href="<?=base_url()?>welcome/logout" style="text-decoration: none; color: #FFF"><p align="right"><i class="fa fa-sign-out-alt"></i> Logout</p></a></li>
				</ul>
			</div>
			<div class="content-dapur">
				<ul class="col-content-dapur">
					<li>
						<div class="box-dapur merah">
							<div class="head-box-dapur">
								<p>Waiting List</p>
							</div>
							<?php
							foreach ($waiting as $waiting) {
							?>
							<div class="item-dapur" onClick="location.href='<?=base_url()?>dapur/det_list_dapur/<?=$waiting->id_pesanan?>/1'">
								<ul class="col-item-dapur">
								<li>
									<span>Meja</span>
									<p><?=$waiting->kd_meja?></p>
								</li>
								<li>
									<span>Menit lalu</span>
									<p><?=$waiting->minute?></p>
								</li>
								<li>
									<span>Quantity</span>
									<p><?=$waiting->qty?></p>
								</li>
							</ul>
							</div>
							<?php
							}
							?>
						</div>
					</li>
					<li>
						<div class="box-dapur orenmuda">
							<div class="head-box-dapur">
								<p>Cooking</p>
							</div>
							<?php
							foreach ($cooking as $cooking) {
							?>
							<div class="item-dapur" data-toggle="modal" data-target="#exampleModalCenter" onClick="location.href='<?=base_url()?>dapur/det_list_dapur/<?=$cooking->id_pesanan?>/2'">
								<ul class="col-item-dapur">
								<li>
									<span>Meja</span>
									<p><?=$cooking->kd_meja?></p>
								</li>
								<li>
									<span>Menit lalu</span>
									<p><?=$cooking->minute?></p>
								</li>
								<li>
									<span>Quantity</span>
									<p><?=$cooking->qty?></p>
								</li>
								</ul>
			                    <div class="progress progress_dapur" style="height: 30px;">
			                      <div class="progress-bar" style="width:<?=$cooking->persen_selesai?>%; background-color: #2ecc71;"></div>
			                    </div>
							</div>
							<?php
							}
							?>
						</div>
					</li>
					<li>
						<div class="box-dapur orentua">
							<div class="head-box-dapur">
								<p>Finishing</p>
							</div>
							<?php
							foreach ($finishing as $finishing) {
							?>
							<div class="item-dapur" data-toggle="modal" data-target="#exampleModalCenter" onClick="location.href='<?=base_url()?>dapur/det_list_dapur/<?=$finishing->id_pesanan?>/3'">
								<ul class="col-item-dapur">
								<li>
									<span>Meja</span>
									<p><?=$finishing->kd_meja?></p>
								</li>
								<li>
									<span>Menit lalu</span>
									<p><?=$finishing->minute?></p>
								</li>
								<li>
									<span>Quantity</span>
									<p><?=$finishing->qty?></p>
								</li>
								</ul>
			                    <div class="progress progress_dapur" style="height: 30px;">
			                      <div class="progress-bar" style="width:<?=$finishing->persen_selesai?>%; background-color: #2ecc71;"></div>
			                    </div>
							</div>
							<?php
							}
							?>
						</div>
					</li>
					<li>
						<div class="box-dapur abu">
							<div class="head-box-dapur">
								<p>Done</p>
							</div>
							<?php
							foreach ($done as $done) {
							?>
							<div class="item-dapur" data-toggle="modal" data-target="#exampleModalCenter" onClick="location.href='<?=base_url()?>dapur/det_list_dapur/<?=$done->id_pesanan?>/4'">
								<ul class="col-item-dapur">
								<li>
									<span>Meja</span>
									<p><?=$done->kd_meja?></p>
								</li>
								<li>
									<span>Menit lalu</span>
									<p><?=$done->minute?></p>
								</li>
								<li>
									<span>Quantity</span>
									<p><?=$done->qty?></p>
								</li>
								</ul>
			                    <div class="progress progress_dapur" style="height: 30px;">
			                      <div class="progress-bar" style="width:<?=$done->persen_selesai?>%; background-color: #2ecc71;"></div>
			                    </div>
							</div>
							<?php
							}
							?>
						</div>
					</li>
				</ul>
			</div>
			<div class="footer-dapur">
				<ul class="col-footer-dapur">
					<li>&nbsp;</li>
					<li>&nbsp;</li>
					<li>&nbsp;</li>
				</ul>
			</div>
		</div>
	</div>