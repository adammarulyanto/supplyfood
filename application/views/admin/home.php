
<style type="text/css">
body{
	background: -moz-linear-gradient(53deg, rgba(52,74,87,1) 0%, rgba(0,33,33,1) 100%); /* ff3.6+ */
	background: -webkit-gradient(linear, left bottom, right top, color-stop(0%, rgba(52,74,87,1)), color-stop(100%, rgba(0,33,33,1))); /* safari4+,chrome */
	background: -webkit-linear-gradient(53deg, rgba(52,74,87,1) 0%, rgba(0,33,33,1) 100%); /* safari5.1+,chrome10+ */
	background: -o-linear-gradient(53deg, rgba(52,74,87,1) 0%, rgba(0,33,33,1) 100%); /* opera 11.10+ */
	background: -ms-linear-gradient(53deg, rgba(52,74,87,1) 0%, rgba(0,33,33,1) 100%); /* ie10+ */
	background: linear-gradient(37deg, rgba(52,74,87,1) 0%, rgba(0,33,33,1) 100%); /* w3c */
	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#002121', endColorstr='#344A57',GradientType=0 ); /* ie6-9 */
}
</style>

	<div class="content-admin">
		<div class="head-content-admin">
			<ul class="col-head-content-admin">
				<li class="active"><span><?=$this->session->userdata('nm')?></span></li>
			</ul>
		</div>
		<div class="body-content-admin">
			<h1>Dashboard</h1>
			<div class="box-count-area">
				<ul class="summary-box-count-list">
					<li>
						<div class="box-count box-count-1">
							<h1><?=$data->user?></h1><span>Anggota</span>
							<button class="btn btn-primary btn-box-count btn-box-count-1" onClick="location.href='<?=base_url()?>admin/anggota'">View detail</button>
						</div>
					</li>
					<li>
						<div class="box-count box-count-1">
							<h1><?=$data->persediaan?></h1><span>Persediaan</span>
							<button class="btn btn-primary btn-box-count btn-box-count-2" onClick="location.href='<?=base_url()?>admin/persediaan'">View detail</button>
						</div>
					</li>
					<li>
						<div class="box-count box-count-1">
							<h1><?=$data->meja?></h1><span>Meja</span>
							<button class="btn btn-primary btn-box-count btn-box-count-3" onClick="location.href='<?=base_url()?>admin/meja'">View detail</button>
						</div>
					</li>
					<li>
						<div class="box-count box-count-1">
							<h1><?=$data->pesanan?></h1><span>Pesanan</span>
							<button class="btn btn-primary btn-box-count btn-box-count-4" onClick="location.href='#'">View detail</button>
						</div>
					</li>
				</ul>
			</div>
		</div>
	</div>
</div>