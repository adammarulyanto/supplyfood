
  <div class="content-admin">
    <div class="head-content-admin">
      <ul class="col-head-content-admin">
      </ul>
    </div>
    <div class="body-content-cetak">
      <div class="head-content-table">
        <div class="left-head-content-table">
          <h1><?=$pesanan->kd_pesanan?></h1> 
          <h2><?=$pesanan->nm_pemesan?></h2>   
          <h5><?=$pesanan->tgl_pesan?></h5>  

        </div>
      </div>
          <table class="table table-cetak">
          <tr>
            <th>No.</th>
            <th>Kode Produk</th>
            <th>Nama Produk</th>
            <th>Chef</th>
            <th>Qty</th>
            <th>Harga</th>
            <th>Status</th>
          </tr>
          <?php
            $no=0;
            foreach ($detail_pesanan as $detail_pesanan){
            $no++;
            ?>
            <tr>
              <td><?=$no?></td>
              <td><?=$detail_pesanan->kd_produk?></td>
              <td><?=$detail_pesanan->nm_produk?></td>
              <td><?=$detail_pesanan->nm_user?></td>
              <td><?=$detail_pesanan->qty?></td>
              <td><?=$detail_pesanan->harga?></td>
              <td>
                <?php
                  if($detail_pesanan->status2 == 'Keranjang'){
                  echo '<span class="flag" style="background:#7f8c8d">';
                  }else if($detail_pesanan->status2 == 'Waiting List'){
                  echo '<span class="flag" style="background:#2980b9">';
                  }else if($detail_pesanan->status2 == 'Cooking'){
                  echo '<span class="flag" style="background:#e67e22">';
                  }else if($detail_pesanan->status2 == 'Finishing'){
                  echo '<span class="flag" style="background:#d35400">';
                  }else if($detail_pesanan->status2 == 'Selesai'){
                  echo '<span class="flag" style="background:#27ae60">';
                  }
                  ?>  
              <?=$detail_pesanan->status2?>
              </span></td>
            </tr>
            <?php
            }
          ?>
          </table> 
    </div>
  </div>
</div>
<script type="text/javascript">
window.print();
</script>