<style type="text/css">
body{
	background: -moz-linear-gradient(53deg, rgba(52,74,87,1) 0%, rgba(0,33,33,1) 100%); /* ff3.6+ */
	background: -webkit-gradient(linear, left bottom, right top, color-stop(0%, rgba(52,74,87,1)), color-stop(100%, rgba(0,33,33,1))); /* safari4+,chrome */
	background: -webkit-linear-gradient(53deg, rgba(52,74,87,1) 0%, rgba(0,33,33,1) 100%); /* safari5.1+,chrome10+ */
	background: -o-linear-gradient(53deg, rgba(52,74,87,1) 0%, rgba(0,33,33,1) 100%); /* opera 11.10+ */
	background: -ms-linear-gradient(53deg, rgba(52,74,87,1) 0%, rgba(0,33,33,1) 100%); /* ie10+ */
	background: linear-gradient(37deg, rgba(52,74,87,1) 0%, rgba(0,33,33,1) 100%); /* w3c */
	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#002121', endColorstr='#344A57',GradientType=0 ); /* ie6-9 */
}
</style>

	<div class="content-admin">
		<div class="head-content-admin">
			<ul class="col-head-content-admin">
				<li class="active"><span><?=$this->session->userdata('nm')?></span></li>
			</ul>
		</div>
		<div class="body-content-admin">
			<div class="head-content-table">
				<div class="left-head-content-table">
					<h1>Edit Anggota</h1>	
				</div>
				<div class="right-head-content-table">
					
				</div>
			</div>
			<div class="content-table">
				<div class="form-data">
					<form action="<?=base_url()?>admin/edit_anggota_act" method="post">
					<label>Username</label>
					<input type="text" name="username" class="form-control form-data-control" value="<?=$anggota->username?>" required=""><br>
					<label>Password</label>
					<input type="password" name="password" class="form-control form-data-control"><br>
					<label>Nama Lengkap</label>
					<input type="text" name="nm_user" class="form-control form-data-control" value="<?=$anggota->nm_user?>" required=""><br>
					<label>Telepon</label>
					<input type="text" name="telp" class="form-control form-data-control" value="<?=$anggota->telp?>" required=""><br>
					<label>Email</label>
					<input type="text" name="email" class="form-control form-data-control" value="<?=$anggota->email?>" required=""><br>
					<label>Level</label>
					<select class="form-control form-data-control" name="level">
						<?php
						foreach ($level as $level) {
						?>
						<option value="<?=$level->id_user_level?>"><?=$level->nm_level?></option>
						<?php
						}
						?>
					</select><br>
					<input type="hidden" name="id" value="<?=$anggota->id_user?>">
					<button class="btn btn-success">Submit</button>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>