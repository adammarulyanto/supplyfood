<style type="text/css">
body{
	background: -moz-linear-gradient(53deg, rgba(52,74,87,1) 0%, rgba(0,33,33,1) 100%); /* ff3.6+ */
	background: -webkit-gradient(linear, left bottom, right top, color-stop(0%, rgba(52,74,87,1)), color-stop(100%, rgba(0,33,33,1))); /* safari4+,chrome */
	background: -webkit-linear-gradient(53deg, rgba(52,74,87,1) 0%, rgba(0,33,33,1) 100%); /* safari5.1+,chrome10+ */
	background: -o-linear-gradient(53deg, rgba(52,74,87,1) 0%, rgba(0,33,33,1) 100%); /* opera 11.10+ */
	background: -ms-linear-gradient(53deg, rgba(52,74,87,1) 0%, rgba(0,33,33,1) 100%); /* ie10+ */
	background: linear-gradient(37deg, rgba(52,74,87,1) 0%, rgba(0,33,33,1) 100%); /* w3c */
	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#002121', endColorstr='#344A57',GradientType=0 ); /* ie6-9 */
}
</style>

	<div class="content-admin">
		<div class="head-content-admin">
			<ul class="col-head-content-admin">
				<li class="active"><span><?=$this->session->userdata('nm')?></span></li>
			</ul>
		</div>
		<div class="body-content-admin">
			<div class="head-content-table">
				<div class="left-head-content-table">
					<h1>Edit Persediaan</h1>	
				</div>
				<div class="right-head-content-table">
					
				</div>
			</div>
			<div class="content-table">
				<div class="form-data">
					<form action="<?=base_url()?>admin/edit_persediaan_act" method="post" enctype="multipart/form-data">
					<input type="hidden" name="id_produk" class="form-control form-data-control" required="" value="<?=$persediaan->id_produk?>">
					<label>Kode Produk</label>
					<input type="text" disabled="" name="kd_produk" class="form-control form-data-control" style="background: rgba(0,0,0,0.1); color: rgba(255,255,255,0.4);" required="" value="<?=$persediaan->kd_produk?>"><br>
					<label>Nama Produk</label>
					<input type="text" name="nm_produk" class="form-control form-data-control" required="" value="<?=$persediaan->nm_produk?>"><br>
					<label>Desc Produk</label>
					<textarea class="form-control form-data-control" style="height: 150px;" name="desc_produk"><?=$persediaan->desc_produk?></textarea><br>
					<label>Gambar</label>
					<input type="file" name="gambar" class="form-control form-data-control">
					<input type="hidden" name="old_gambar" value="<?=$persediaan->gambar?>"><br>
					*jika tidak di isi akan menggunakan file sebelumnya<br><br>
					<label>Harga (Rp.)</label>
					<input type="number" name="harga" class="form-control form-data-control" required="" value="<?=$persediaan->harga?>"><br>
					<label>Jenis Produk</label>
					<select class="form-control form-data-control" name="jproduk">
						<?php
						foreach ($jproduk as $jproduk) {
						?>
						<option value="<?=$jproduk->id_jenis_produk?>"><?=$jproduk->nm_jenis_produk?></option>
						<?php
						}
						?>
					</select><br>
					<button class="btn btn-success">Submit</button>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>