
  <div class="content-admin">
    <div class="head-content-admin">
      <ul class="col-head-content-admin">
      </ul>
    </div>
    <div class="body-content-cetak">
      <div class="head-content-table">
        <div class="left-head-content-table">
          <h1>Daftar Pembayaran</h1> 
          <h5><?=date('Y M d')?></h5>
        </div>
      </div>
          <table class="table table-cetak">
          <tr>
            <th>No.</th>
            <th>Kode Pesanan</th>
            <th>Tagihan/th>
            <th>Nominal Bayar</th>
            <th>Kembalian</th>
            <th>Status</th>
            <th>Tgl Bayar</th>
          </tr>
          <?php
            $no=0;
            foreach ($pembayaran as $pembayaran){
            $no++;
            ?>
            <tr>
              <td><?=$no?></td>
              <td><?=$pembayaran->kd_pesanan?></td>
              <td><?=$pembayaran->total_tagihan?></td>
              <td><?=$pembayaran->nominal_bayar?></td>
              <td><?=$pembayaran->kembali?></td>
              <td>
                <?php
                  if($pembayaran->status2 == '1'){
                  echo '<span class="flag" style="background:#7f8c8d">';
                  }else if($pembayaran->status2 == '2'){
                  echo '<span class="flag" style="background:#2980b9">';
                  }
                  ?>  
              <?=$pembayaran->status_pembayaran?>
              </span></td>
              <td><?=$pembayaran->tgl_bayar?></td>
            </tr>
            <?php
            }
          ?>
          </table> 
    </div>
  </div>
</div>
<script type="text/javascript">
window.print();
</script>