<div class="col-dashboard-admin">
	<div class="sidemenu">
		<ul class="sidemenu-list">
			<a href="<?=base_url()?>admin"><li><i class="fa fa-tachometer-alt"></i><br><span>Dashboard</span></li></a>
			<a href="<?=base_url()?>admin/anggota"><li><i class="fa fa-users"></i><br><span>Anggota</span></li></a>
			<a href="<?=base_url()?>admin/persediaan"><li><i class="fa fa-utensils"></i><br><span>Persediaan</span></li></a>
			<a href="<?=base_url()?>admin/meja"><li><i class="fa fa-chair"></i><br><span>Meja</span></li></a>
			<a href="<?=base_url()?>admin/transaksi"><li><i class="fas fa-copy"></i><br><span>Transaksi</span></li></a>
			<a href="<?=base_url()?>welcome/logout"><li style="position: absolute;bottom: 10px;left: 20px"><i class="fa fa-sign-out-alt"></i><br><span>Logout</span></li></a>
		</ul>
	</div>