<style type="text/css">
body{
	background: -moz-linear-gradient(53deg, rgba(52,74,87,1) 0%, rgba(0,33,33,1) 100%); /* ff3.6+ */
	background: -webkit-gradient(linear, left bottom, right top, color-stop(0%, rgba(52,74,87,1)), color-stop(100%, rgba(0,33,33,1))); /* safari4+,chrome */
	background: -webkit-linear-gradient(53deg, rgba(52,74,87,1) 0%, rgba(0,33,33,1) 100%); /* safari5.1+,chrome10+ */
	background: -o-linear-gradient(53deg, rgba(52,74,87,1) 0%, rgba(0,33,33,1) 100%); /* opera 11.10+ */
	background: -ms-linear-gradient(53deg, rgba(52,74,87,1) 0%, rgba(0,33,33,1) 100%); /* ie10+ */
	background: linear-gradient(37deg, rgba(52,74,87,1) 0%, rgba(0,33,33,1) 100%); /* w3c */
	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#002121', endColorstr='#344A57',GradientType=0 ); /* ie6-9 */
}
</style>

	<div class="content-admin">
		<div class="head-content-admin">
			<ul class="col-head-content-admin">
				<li class="active"><span><?=$this->session->userdata('nm')?></span></li>
			</ul>
		</div>
		<div class="body-content-admin">
			<div class="head-content-table">
				<div class="left-head-content-table">
					<h1><?=$pesanan->kd_pesanan?></h1>
					<h2><?=$pesanan->nm_pemesan?></h2>
					<h5><?=$pesanan->tgl_pesan?></h5>	
					<p>
				</div>
				<div class="right-head-content-table">
					<button class="btn btn-primary btn-add" onclick="window.location.href='<?=base_url()?>admin/cetak_detail_pesanan/<?=$pesanan->id_pesanan?>'"><i class="fa fa-print"></i> Cetak</button>
				</div>
			</div>
			<div class="content-table">
				<div class="dhead">
					<ul class="ul-data-table">
						<li style="width: 5%;">No.</li>
						<li style="width: 25%">Kode Produk</li>
						<li style="width: 25%">Nama Produk</li>
						<li style="width: 10%">Chef</li>
						<li style="width: 10%">Qty</li>
						<li style="width: 10%">Harga</li>
						<li style="width: 15%">Status</li>
					</ul>
				</div>
				<div class="dbody">
					<?php
						$no=0;
						foreach ($detail_pesanan as $detail_pesanan){
						$no++;
						?>
						<ul class="ul-data-table">
							<li style="width: 5%; border-radius: 5px 0 0 5px;"><?=$no?></li>
							<li style="width: 25%"><?=$detail_pesanan->kd_produk?></li>
							<li style="width: 25%"><?=$detail_pesanan->nm_produk?></li>
							<li style="width: 10%"><?=$detail_pesanan->nm_user?></li>
							<li style="width: 10%"><?=$detail_pesanan->qty?></li>
							<li style="width: 10%"><?=$detail_pesanan->harga?></li>
							<li style="width: 15%">
								<?php
									if($detail_pesanan->status2 == 'Keranjang'){
									echo '<span class="flag" style="background:#7f8c8d">';
									}else if($detail_pesanan->status2 == 'Waiting List'){
									echo '<span class="flag" style="background:#2980b9">';
									}else if($detail_pesanan->status2 == 'Cooking'){
									echo '<span class="flag" style="background:#e67e22">';
									}else if($detail_pesanan->status2 == 'Finishing'){
									echo '<span class="flag" style="background:#d35400">';
									}else if($detail_pesanan->status2 == 'Selesai'){
									echo '<span class="flag" style="background:#27ae60">';
									}
									?>	
							<?=$detail_pesanan->status2?>
							</span></li>
						</ul>
						<?php
						}
					?>
						
				</div>
			</div>
		</div>
	</div>
</div>