<style type="text/css">
body{
	background: -moz-linear-gradient(53deg, rgba(52,74,87,1) 0%, rgba(0,33,33,1) 100%); /* ff3.6+ */
	background: -webkit-gradient(linear, left bottom, right top, color-stop(0%, rgba(52,74,87,1)), color-stop(100%, rgba(0,33,33,1))); /* safari4+,chrome */
	background: -webkit-linear-gradient(53deg, rgba(52,74,87,1) 0%, rgba(0,33,33,1) 100%); /* safari5.1+,chrome10+ */
	background: -o-linear-gradient(53deg, rgba(52,74,87,1) 0%, rgba(0,33,33,1) 100%); /* opera 11.10+ */
	background: -ms-linear-gradient(53deg, rgba(52,74,87,1) 0%, rgba(0,33,33,1) 100%); /* ie10+ */
	background: linear-gradient(37deg, rgba(52,74,87,1) 0%, rgba(0,33,33,1) 100%); /* w3c */
	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#002121', endColorstr='#344A57',GradientType=0 ); /* ie6-9 */
}
</style>

	<div class="content-admin">
		<div class="head-content-admin">
			<ul class="col-head-content-admin">
				<li class="active"><span><?=$this->session->userdata('nm')?></span></li>
			</ul>
		</div>
		<div class="body-content-admin">
			<div class="head-content-table">
				<div class="left-head-content-table">
					<h1>Tambah Meja</h1>	
				</div>
				<div class="right-head-content-table">
					
				</div>
			</div>
			<div class="content-table">
				<div class="form-data">
					<form action="<?=base_url()?>admin/add_meja_act" method="post">
					<label>Kode Meja</label>
					<input type="text" name="kd_meja" class="form-control form-data-control" required=""><br>
					<label>User</label>
					<select class="form-control form-data-control" name="usermeja" required="">
						<?php
						foreach ($usermeja as $usermeja) {
						?>
						<option value="<?=$usermeja->id_user?>"><?=$usermeja->nm_user?></option>
						<?php
						}
						?>
					</select><br>
					<button class="btn btn-success">Submit</button>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>