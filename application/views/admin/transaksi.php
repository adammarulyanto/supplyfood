<style type="text/css">
body{
	background: -moz-linear-gradient(53deg, rgba(52,74,87,1) 0%, rgba(0,33,33,1) 100%); /* ff3.6+ */
	background: -webkit-gradient(linear, left bottom, right top, color-stop(0%, rgba(52,74,87,1)), color-stop(100%, rgba(0,33,33,1))); /* safari4+,chrome */
	background: -webkit-linear-gradient(53deg, rgba(52,74,87,1) 0%, rgba(0,33,33,1) 100%); /* safari5.1+,chrome10+ */
	background: -o-linear-gradient(53deg, rgba(52,74,87,1) 0%, rgba(0,33,33,1) 100%); /* opera 11.10+ */
	background: -ms-linear-gradient(53deg, rgba(52,74,87,1) 0%, rgba(0,33,33,1) 100%); /* ie10+ */
	background: linear-gradient(37deg, rgba(52,74,87,1) 0%, rgba(0,33,33,1) 100%); /* w3c */
	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#002121', endColorstr='#344A57',GradientType=0 ); /* ie6-9 */
}
</style>

	<div class="content-admin">
		<div class="head-content-admin">
			<ul class="col-head-content-admin">
				<li class="active"><span><?=$this->session->userdata('nm')?></span></li>
			</ul>
		</div>
		<div class="body-content-admin">
			<div class="head-content-table">
				<div class="left-head-content-table">
					<h1>Transaksi</h1>	
				</div>
				<div class="right-head-content-table">
				</div>
			</div>
			<div class="content-table">
				<div class="kiri-laporan">
				<div class="sub-judul">
				<h2>Pesanan</h2>
				<a href="<?=base_url()?>admin/cetak_pesanan" alt="Print" class="tmbl-cetak">Cetak <i class="fa fa-print"></i></a>
				</div>
					<div class="dhead">
						<ul class="ul-data-table">
							<li style="width: 5%;">No.</li>
							<li style="width: 25%">Kode</li>
							<li style="width: 35%">Status</li>
						</ul>
					</div>
					<div class="dbody">
						<?php
							$no=0;
							foreach ($pesanan as $pesanan){
							$no++;
							?>
							<ul class="ul-data-table">
								<li style="width: 5%; border-radius: 5px 0 0 5px;"><?=$no?></li>
								<li style="width: 25%"><?=$pesanan->kd_pesanan?></li>
								<li style="width: 35%">
									<?php
									if($pesanan->status == '1'){
									echo '<span class="flag" style="background:#2ecc71">';
									}else if ($pesanan->status == '2'){
									echo '<span class="flag" style="background:#3498db">';
									}else if ($pesanan->status == '0'){
									echo '<span class="flag" style="background:#c0392b">';
									}
									?>	
									<?=$pesanan->status_pesanan?>
									</span></li>
								<li style="width: 32%; text-align: right; border-radius: 0 5px 5px 0;">
									<a href="<?=base_url()?>admin/detail_pesanan/<?=$pesanan->id_pesanan?>" alt="Batal"><i class="fa fa-eye"></i></a> &nbsp;
									<?php
									if($pesanan->status == '1'){
									?>
									<a href="<?=base_url()?>admin/batal_pesanan/<?=$pesanan->id_pesanan?>" alt="Batal"><i class="fa fa-minus-square"></i></a>
									<?php
									}
									?>	
								</li>
							</ul>
							<?php
							}
						?>		
					</div>
				</div>
				<div class="kanan-laporan">
				<div class="sub-judul">
				<h2>Pembayaran</h2>
				<a href="<?=base_url()?>admin/cetak_pembayaran" alt="Print" class="tmbl-cetak">Cetak <i class="fa fa-print"></i></a>
				</div>
					<div class="dhead">
						<ul class="ul-data-table" style="width:100%">
							<li style="width: 5%;">No.</li>
							<li style="width: 25%">Kode Pembayaran</li>
							<li style="width: 25%">Kode Pesanan</li>
							<li style="width: 25%">Status</li>
							<li style="width: 25%"></li>
						</ul>
					</div>
					<div class="dbody">
						<?php
							$no=0;
							foreach ($pembayaran as $pembayaran){
							$no++;
							?>
							<ul class="ul-data-table">
								<li style="width: 5%; border-radius: 5px 0 0 5px;"><?=$no?></li>
								<li style="width: 25%"><?=$pembayaran->kd_pembayaran?></li>
								<li style="width: 25%"><?=$pembayaran->kd_pesanan?></li>
								<li style="width: 20%">
									<?php
									if($pembayaran->status2 == '1'){
									echo '<span class="flag" style="background:#2ecc71">';
									}else if ($pembayaran->status2 == '2'){
									echo '<span class="flag" style="background:#3498db">';
									}else if ($pembayaran->status2 == '0'){
									echo '<span class="flag" style="background:#c0392b">';
									}
									?>	
									<?=$pembayaran->status_pesanan?>
									</span></li>
								<li style="width: 20%; text-align: left; border-radius: 0 5px 5px 0;">
									<a href="<?=base_url()?>admin/detail_pembayaran/<?=$pembayaran->kd_pembayaran?>" alt="Batal"><i class="fa fa-eye"></i></a> &nbsp;
									<a href="<?=base_url()?>admin/batal_bayar/<?=$pembayaran->id_pembayaran?>" alt="Batal"><i class="fa fa-minus-square"></i></a>
								</li>
							</ul>
							<?php
							}
						?>		
					</div>
				</div>
			</div>
		</div>
	</div>
</div>