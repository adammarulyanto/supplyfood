
  <div class="content-admin">
    <div class="head-content-admin">
      <ul class="col-head-content-admin">
      </ul>
    </div>
    <div class="body-content-cetak">
      <div class="head-content-table">
        <div class="left-head-content-table">
          <h1><?=$pembayaran->kd_pembayaran?></h1> 
          <h2><?=$pembayaran->nm_pemesan?></h2> - <h2><?=$pembayaran->kd_pesanan?></h2>   
          <h5><?=$pembayaran->tgl_bayar?></h5>  
          <hr width="300px" align="left">
          <p>Total Tagihan : Rp. <?=$pembayaran->total_tagihan?></p>
          <p>Nominal Pembayaran : Rp. <?=$pembayaran->nominal_bayar?></p>
          <p>Kembalian : Rp. <?=$pembayaran->kembali?></p>
        </div>
      </div>
          <table class="table table-cetak">
          <tr>
            <th>No.</th>
            <th>Kode Produk</th>
            <th>Nama Produk</th>
            <th>Chef</th>
            <th>Qty</th>
            <th>Harga</th>
            <th>Status</th>
          </tr>
          <?php
            $no=0;
            foreach ($detail_pesanan as $detail_pesanan){
            $no++;
            ?>
            <tr>
              <td><?=$no?></td>
              <td><?=$detail_pesanan->kd_produk?></td>
              <td><?=$detail_pesanan->nm_produk?></td>
              <td><?=$detail_pesanan->nm_user?></td>
              <td><?=$detail_pesanan->qty?></td>
              <td><?=$detail_pesanan->harga?></td>
              <td>
                <?php
                  if($detail_pesanan->status_pembayaran == '1'){
                  echo '<span class="flag" style="background:#7f8c8d">';
                  }else if($detail_pesanan->status_pembayaran == '2'){
                  echo '<span class="flag" style="background:#2980b9">';
                  }
                  ?>  
              <?=$detail_pesanan->status2?>
              </span></td>
            </tr>
            <?php
            }
          ?>
          </table> 
    </div>
  </div>
</div>
<script type="text/javascript">
window.print();
</script>