<style type="text/css">
body{
	background: -moz-linear-gradient(53deg, rgba(52,74,87,1) 0%, rgba(0,33,33,1) 100%); /* ff3.6+ */
	background: -webkit-gradient(linear, left bottom, right top, color-stop(0%, rgba(52,74,87,1)), color-stop(100%, rgba(0,33,33,1))); /* safari4+,chrome */
	background: -webkit-linear-gradient(53deg, rgba(52,74,87,1) 0%, rgba(0,33,33,1) 100%); /* safari5.1+,chrome10+ */
	background: -o-linear-gradient(53deg, rgba(52,74,87,1) 0%, rgba(0,33,33,1) 100%); /* opera 11.10+ */
	background: -ms-linear-gradient(53deg, rgba(52,74,87,1) 0%, rgba(0,33,33,1) 100%); /* ie10+ */
	background: linear-gradient(37deg, rgba(52,74,87,1) 0%, rgba(0,33,33,1) 100%); /* w3c */
	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#002121', endColorstr='#344A57',GradientType=0 ); /* ie6-9 */
}
</style>

	<div class="content-admin">
		<div class="head-content-admin">
			<ul class="col-head-content-admin">
				<li class="active"><span><?=$this->session->userdata('nm')?></span></li>
			</ul>
		</div>
		<div class="body-content-admin">
			<div class="head-content-table">
				<div class="left-head-content-table">
					<h1>Anggota</h1>	
				</div>
				<div class="right-head-content-table">
					<button class="btn btn-primary btn-add" onClick="location.href='<?=base_url()?>admin/add_anggota'">+ Add</button>
				</div>
			</div>
			<div class="content-table">
				<div class="dhead">
					<ul class="ul-data-table">
						<li style="width: 5%;">No.</li>
						<li style="width: 20%">Nama</li>
						<li style="width: 10%">Username</li>
						<li style="width: 20%">Email</li>
						<li style="width: 20%">Level</li>
						<li style="width: 10%">Status</li>
						<li style="width: 15%; text-align: right;">&nbsp;</li>
					</ul>
				</div>
				<div class="dbody">
					<?php
						$no=0;
						foreach ($anggota as $anggota){
						$no++;
						?>
						<ul class="ul-data-table">
							<li style="width: 5%;border-radius: 5px 0 0 5px;"><?=$no?></li>
							<li style="width: 20%"><?=$anggota->nm_user?></li>
							<li style="width: 10%"><?=$anggota->username?></li>
							<li style="width: 20%"><?=$anggota->email?></li>
							<li style="width: 20%"><?=$anggota->nm_level?></li>
							<li style="width: 10%">
									<?php
									if($anggota->status_user == 'Active'){
									echo '<span class="flag" style="background:#27ae60">';
									}else{
									echo '<span class="flag" style="background:#c0392b">';
									}
									?>	
							<?=$anggota->status_user?>
							</span>
							</li>
							<li style="width: 15%;text-align: right; border-radius: 0 5px 5px 0;">
								<a href="<?=base_url()?>admin/edit_anggota/<?=$anggota->id_user?>" alt="Edit"><i class="fa fa-edit"></i></a>&nbsp;
								<a href="<?=base_url()?>admin/inactive_anggota/<?=$anggota->id_user?>" alt="Edit"><i class="fa fa-minus-square"></i></a>
								<!-- <a href="<?=base_url()?>admin/delete_anggota/<?=$anggota->id_user?>" alt="Hapus"><i class="fa fa-minus-square"></i></a> -->
							</li>
						</ul>
						<?php
						}
					?>
						
				</div>
			</div>
		</div>
	</div>
</div>