
  <div class="content-admin">
    <div class="head-content-admin">
      <ul class="col-head-content-admin">
      </ul>
    </div>
    <div class="body-content-cetak">
      <div class="head-content-table">
        <div class="left-head-content-table">
          <h1>Daftar Pesanan</h1> 
          <h5><?=date('Y M d')?></h5>
        </div>
      </div>
          <table class="table table-cetak">
          <tr>
            <th>No.</th>
            <th>Kode Pesanan</th>
            <th>Nama Pemesan</th>
            <th>Meja</th>
            <th>Status</th>
            <th>Tgl Pesan</th>
          </tr>
          <?php
            $no=0;
            foreach ($pesanan as $pesanan){
            $no++;
            ?>
            <tr>
              <td><?=$no?></td>
              <td><?=$pesanan->kd_pesanan?></td>
              <td><?=$pesanan->nm_pemesan?></td>
              <td><?=$pesanan->kd_meja?></td>
              <td>
                <?php
                  if($pesanan->status == '1'){
                  echo '<span class="flag" style="background:#7f8c8d">';
                  }else if($pesanan->status == '2'){
                  echo '<span class="flag" style="background:#2980b9">';
                  }else if($pesanan->status == '0'){
                  echo '<span class="flag" style="background:#e67e22">';
                  }
                  ?>  
              <?=$pesanan->status2?>
              </span></td>
              <td><?=$pesanan->tgl_pesan?></td>
            </tr>
            <?php
            }
          ?>
          </table> 
    </div>
  </div>
</div>
<script type="text/javascript">
window.print();
</script>