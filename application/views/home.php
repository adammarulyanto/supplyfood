      <div class="container-fluid">
        <div class="head">
          <ul class="head-logo">
            <li class="head-left"><img class="logo" src="<?=base_url()?>/assets/images/logo.png"></li>
            <li class="head-right"><span class="kotak-meja">No Meja : A42</span></li>
          </ul>
        </div>
        <div class="content-app">
          <div class="form-group">
          <h1>Pesan</h1>
          <input type="text" class="form-control form-nama-pemesan" placeholder="Masukan Nama Anda" name="nama_pemesan">
            <small id="emailHelp" class="form-text text-muted">Nama anda digunakan untuk mendata pesanan.</small>
          </div>
          <button type="submit" class="btn btn-primary">Next</button>


          <div class="call-support">
          <h4>Butuh Bantuan?</h4>
          <button type="submit" class="btn btn-primary btn-help">Panggil Bantuan</button>
          </div>
        </div>
      </div>
    