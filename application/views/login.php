<style type="text/css">
body{
background: -moz-linear-gradient(91deg, rgba(33,73,122,1) 0%, rgba(0,128,128,1) 100%); /* ff3.6+ */
background: -webkit-gradient(linear, left top, left bottom, color-stop(0%, rgba(0,128,128,1)), color-stop(100%, rgba(33,73,122,1))); /* safari4+,chrome */
background: -webkit-linear-gradient(91deg, rgba(33,73,122,1) 0%, rgba(0,128,128,1) 100%); /* safari5.1+,chrome10+ */
background: -o-linear-gradient(91deg, rgba(33,73,122,1) 0%, rgba(0,128,128,1) 100%); /* opera 11.10+ */
background: -ms-linear-gradient(91deg, rgba(33,73,122,1) 0%, rgba(0,128,128,1) 100%); /* ie10+ */
background: linear-gradient(359deg, rgba(33,73,122,1) 0%, rgba(0,128,128,1) 100%); /* w3c */
filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#008080', endColorstr='#21497A',GradientType=0 ); /* ie6-9 */
}
</style>

<!-- no additional media querie or css is required -->
	<div class="container">
    	<div class="row justify-content-center align-items-center" style="height:100vh">
        	<div class="col-sm-12 col-lg-4">
                	<div class="card-body">
                        <div class="head-form">
                    		<h1>Welcome</h1>
                            <p>Sign in untuk melanjutkan</p>
                        </div>
                    	<form action="<?=base_url()?>login/login_act" autocomplete="off" method="post">
                            <div class="form-group">
                                <span class="label-form">Username</span>
                                <input type="text" class="form-control form-login" name="username" required="">
                            </div>
                            <div class="form-group">
                                <span class="label-form">Password</span>
                                <input type="password" class="form-control form-login" name="password" required="">
                            </div>
                            <button type="submit" id="sendlogin" class="btn btn-primary btn-login">login</button>
                        </form>
                    </div>
            </div>
        </div>
    </div>
