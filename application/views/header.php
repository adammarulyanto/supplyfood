<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Supplyfood</title>

  <!-- Bootstrap core CSS -->
  <link href="<?=base_url()?>/assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

  <!-- font awesome -->
  <link href="<?=base_url()?>/assets/vendor/font-awesome/css/all.css" rel="stylesheet">

  <!-- Custom styles for this template -->
  <link href="<?=base_url()?>/assets/css/simple-sidebar.css" rel="stylesheet">
  <link href="<?=base_url()?>/assets/css/style.css" rel="stylesheet">

  <!-- Font -->
  <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Baloo+Bhai" rel="stylesheet">
</head>

<body>

  <div class="d-flex" id="wrapper">

   

      