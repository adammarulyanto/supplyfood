<?php
if(isset($_GET['alert'])){
    if($_GET['alert']=='belum_login'){
        $page=$this->uri->segment('1');
?>
<div class="modal modal-popup">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" onClick="location.href='<?=base_url()?><?=$page?>'"><span>×</span></button>
                     </div>
					
                    <div class="modal-body">
                       
						<div class="thank-you-pop">
                            <h1 style="font-size: 100px;color: #d35400"><i class="fa fa-exclamation-circle"></i></h1>
							<h1>Belum Login</h1>
							<p>Masukan Username dan Password terlebih dahulu</p>
 						</div>
                         
                    </div>
					
                </div>
            </div>
        </div>
<?php
}else if($_GET['alert']=='gagal_login'){
        $page=$this->uri->segment('1');
?>
<div class="modal modal-popup">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" onClick="location.href='<?=base_url()?><?=$page?>'"><span>×</span></button>
                     </div>
                    
                    <div class="modal-body">
                       
                        <div class="thank-you-pop">
                            <h1 style="font-size: 100px;color: #d35400"><i class="fa fa-exclamation-circle"></i></h1>
                            <h1>Gagal Login</h1>
                            <p>Masukan Username dan Password dengan tepat</p>
                        </div>
                         
                    </div>
                    
                </div>
            </div>
        </div>
<?php
}else if($_GET['alert']=='pilih_meja'){
        $page=$this->uri->segment('1');
?>
<div class="modal modal-popup">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" onClick="location.href='<?=base_url()?><?=$page?>'"><span>×</span></button>
                     </div>
                    
                    <div class="modal-body">
                       
                        <div class="thank-you-pop">
                            <h1 style="font-size: 100px;color: #d35400"><i class="fa fa-exclamation-circle"></i></h1>
                            <h1>Gagal Login</h1>
                            <p>Mohon hubungi Admin, karena akun meja ini belum memiliki meja</p>
                        </div>
                         
                    </div>
                    
                </div>
            </div>
        </div>
<?php
}
}
?>