<!-- no additional media querie or css is required -->
  <div class="container">
      <div class="row justify-content-center align-items-center" style="height:100vh">
          <div class="col-sm-12 col-lg-5">
                  <div class="card-body-pembayaran">
                    <div class="head-form">
                      <a href="<?=base_url()?>kasir"><h1><?=$bayar->kd_pesanan?></h1></a>
                      <p><?=$bayar->nm_pemesan?></p>
                    </div>
				    <div class="content-dashboard-kasir">
				    		<div class="list-bayar-area">
				    		<?php
                        		foreach ($list_bayar as $list_bayar) {
	                      	?>
		                        <hr>
		                        <div class="list-bar-menu-det-bayar" onClick="document.location.href='#'">
		                        <h5><?=$list_bayar->nm_produk?></h5>
		                        <p>Quantity : <?=$list_bayar->qty?></p>
		                        <p class="harga-bayar">Rp. <?=$list_bayar->harga?></p>
		                        </div>
		                        <hr>
	                      	<?php
	                      	}
	                      	?>
	                  		</div>
	                      <hr>
	                      <div class="list-bar-menu-det-bayar" onClick="document.location.href='#'">
	                        <h5>Sub Total</h5>
	                        <p class="total-bayar">Rp. <?=$bayar->harga?></p>
	                      </div>
	                      <hr>
	                      <div class="list-bar-menu-det-bayar" onClick="document.location.href='#'">
	                        <h5>Nominal Bayar</h5>
	                        <p class="total-bayar">Rp. <?=number_format($bayar->nominal_bayar)?></p>
	                      </div>
	                      <div class="list-bar-menu-det-bayar" onClick="document.location.href='#'">
	                        <h5>Total Tagihan</h5>
	                        <p class="total-bayar">Rp. <?=$bayar->total_bayar?></p>
	                      </div>
	                      <div class="list-bar-menu-det-bayar" onClick="document.location.href='#'">
	                        <h5>Kembali</h5>
	                        <p class="total-bayar">Rp. <?=number_format($bayar->kembali)?></p>
	                      </div><br>
	                      <form method="post" action="<?=base_url()?>kasir/pesanan_lunas/<?=$bayar->kd_pembayaran?>">
                          	<button type="submit" id="sendlogin" class="btn btn-primary btn-bayar">Selesai</button>
                      	  </form>
				    </div>
                    </div>
            </div>
        </div>
    </div>