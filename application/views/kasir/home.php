<style type="text/css">
body{
background: -moz-linear-gradient(45deg, rgba(26,6,6,1) 0%, rgba(14,0,84,1) 100%); /* ff3.6+ */
background: -webkit-gradient(linear, left bottom, right top, color-stop(0%, rgba(26,6,6,1)), color-stop(100%, rgba(14,0,84,1))); /* safari4+,chrome */
background: -webkit-linear-gradient(45deg, rgba(26,6,6,1) 0%, rgba(14,0,84,1) 100%); /* safari5.1+,chrome10+ */
background: -o-linear-gradient(45deg, rgba(26,6,6,1) 0%, rgba(14,0,84,1) 100%); /* opera 11.10+ */
background: -ms-linear-gradient(45deg, rgba(26,6,6,1) 0%, rgba(14,0,84,1) 100%); /* ie10+ */
background: linear-gradient(45deg, rgba(26,6,6,1) 0%, rgba(14,0,84,1) 100%); /* w3c */
filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#0E0054', endColorstr='#1A0606',GradientType=1 ); /* ie6-9 */
}
</style>

<!-- no additional media querie or css is required -->
  <div class="container">
      <div class="row justify-content-center align-items-center" style="height:100vh">
          <div class="col-sm-12 col-lg-5">
                  <div class="card-body">
                    <div class="head-form">
                      <h1><?=$this->session->userdata('nm')?></h1>
                      <p>Kasir</p>
                      <a href="<?=base_url()?>welcome/logout" class="btn btn-primary btn-logout-kasir">Logout</a>
                    </div>
				    <div class="content-dashboard-kasir">
				    	<ul class="col-content-kasir">
				    		<a href="<?=base_url()?>kasir/waiting_list">
					    		<li>
					    			<div class="box-dashboard-kasir box-1-kasir">
					    				<h1><?=$portofolio->waiting?></h1>
					    				<span>Waiting List</span>
					    			</div>
					    		</li>
				    		</a>
				    		<a href="<?=base_url()?>kasir/dapur_list">
				    		<li>
				    			<div class="box-dashboard-kasir box-2-kasir">
				    				<h1><?=$portofolio->dapur?></h1>
				    				<span>Dapur</span>
				    			</div>
				    		</li>
				    		</a>
				    		<a href="<?=base_url()?>kasir/done_list">
				    		<li>
				    			<div class="box-dashboard-kasir box-4-kasir">
				    				<h1><?=$portofolio->done?></h1>
				    				<span>Selesai</span>
				    			</div>
				    		</li>
				    		</a>
				    		<a href="<?=base_url()?>kasir/pembayaran">
				    		<li>
				    			<div class="box-dashboard-kasir box-3-kasir">
				    				<h1><?=$portofolio->pembayaran?></h1>
				    				<span>Pembayaran</span>
				    			</div>
				    		</li>
				    		</a>
				    	</ul>
				    </div>
                    </div>
            </div>
        </div>
    </div>