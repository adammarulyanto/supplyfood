<!-- no additional media querie or css is required -->
  <div class="container">
      <div class="row justify-content-center align-items-center" style="height:100vh">
          <div class="col-sm-12 col-lg-5">
                  <div class="card-body-pembayaran">
                    <div class="head-form">
                      <a href="<?=base_url()?>kasir"><h1><i class="fa fa-chevron-left" style="font-size: 35px"></i> &nbsp;Pembayaran</h1></a>
                      <p>List Pesanan yang belum terbayar</p>
                    </div>
				    <div class="content-dashboard-kasir">
				    	<div class="search-bar">
				    		<i class="fa fa-search"></i>
				    		<form>
				    			<input type="text" name="kd_pesanan" class="form-control form-search-bayar" autocomplete="off" placeholder="Masukan No. Pesanan">
				    		</form>
				    	</div>
				    	<?php
                         foreach ($list_bayar as $list_bayar) {
	                      ?>
	                        <hr>
	                        <div class="list-bar-menu-bayar" onClick="document.location.href='<?=base_url()?>kasir/det_pembayaran/<?=$list_bayar->kd_pembayaran?>'">
	                        <h3><?=$list_bayar->kd_pembayaran?></h3>
	                        <p><?=$list_bayar->nm_pemesan?> - <?=$list_bayar->kd_pesanan?></p>
	                        </div>
	                        <hr>
	                      <?php
	                      }
	                      ?>
				    </div>
                    </div>
            </div>
        </div>
    </div>