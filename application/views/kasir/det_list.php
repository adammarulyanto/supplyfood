<style type="text/css">
body{
background: -moz-linear-gradient(45deg, rgba(26,6,6,1) 0%, rgba(14,0,84,1) 100%); /* ff3.6+ */
background: -webkit-gradient(linear, left bottom, right top, color-stop(0%, rgba(26,6,6,1)), color-stop(100%, rgba(14,0,84,1))); /* safari4+,chrome */
background: -webkit-linear-gradient(45deg, rgba(26,6,6,1) 0%, rgba(14,0,84,1) 100%); /* safari5.1+,chrome10+ */
background: -o-linear-gradient(45deg, rgba(26,6,6,1) 0%, rgba(14,0,84,1) 100%); /* opera 11.10+ */
background: -ms-linear-gradient(45deg, rgba(26,6,6,1) 0%, rgba(14,0,84,1) 100%); /* ie10+ */
background: linear-gradient(45deg, rgba(26,6,6,1) 0%, rgba(14,0,84,1) 100%); /* w3c */
filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#0E0054', endColorstr='#1A0606',GradientType=1 ); /* ie6-9 */
}
</style>

<!-- no additional media querie or css is required -->
  <div class="container">
      <div class="row justify-content-center align-items-center" style="height:100vh">
          <div class="col-sm-12 col-lg-6">
                  <div class="card-body">
                    <p>Detail Pesanan</p>
                    <h1><?=$det->nm_pemesan?></h1>
                    <p><?=$det->kd_pesanan?></p>
                    <p><?=$det->kd_meja?></p>
                      <div class="box-list">
                        <div class="list-bar-menu-dapur head-det_dapur" onClick="document.location.href='#'">
                          <ul class="main-menu-bar-menu">
                            <li>Nama</li>
                            <li>Qty</li>
                            <li></li>
                          </ul>
                        </div>
                      <?php
                         foreach ($det_pesanan as $det_pesanan) {
                      ?>
                        <div class="list-bar-menu-waiting" onClick="document.location.href='#'">
                          <ul class="main-menu-bar-menu">
                            <li><?=$det_pesanan->nm_produk?></li>
                            <li><?=$det_pesanan->qty?></li>
                            <li>
                              <?php
                              if($det_pesanan->status=='0'){
                              ?>
                                <p> <?=anchor(base_url().'kasir/update_status/'.$det_pesanan->id_detail_pesanan,'Push to Dapur',
                                [
                                  'class' => 'btn btn-primary flag-pesan',
                                  'role'  => 'button'
                                ])?>
                                </p>
                              <?php
                              }else if($det_pesanan->status=='1'){
                              ?>
                               <button class="btn btn-primary flag-pesan">Waiting List</button>
                              <?php
                              }else if($det_pesanan->status=='2'){
                              ?>
                               <button class="btn btn-warning flag-pesan">Cooking</button>
                              <?php
                              }else if($det_pesanan->status=='3'){
                              ?>
                               <button class="btn btn-warning flag-pesan">Finishing</button>
                              <?php
                              }else{
                              ?>
                                <span class="btn btn-success flag-pesan">Selesai</span>
                              <?php
                              }
                              ?>
                            </li>
                          </ul>
                        </div>
                      <?php
                      }
                      ?>
                      </div>
                      <ul class="box-btn-waiting">
                        <li><button type="submit" id="sendlogin" class="btn btn-primary btn-pembayaran1" onClick="document.location.href='<?=$_SERVER["HTTP_REFERER"]?>'"><i class="fa fa-chevron-left"></i> &nbsp;&nbsp;Back</button></li>
                        <li><button type="submit" id="sendlogin" class="btn btn-pembayaran1 btn-midnightblue" onClick="document.location.href='<?=base_url()?>kasir/push_all/<?=$det_pesanan->id_pesanan?>'">Push All</button></li>
                      </ul>
                    </div>
            </div>
        </div>
    </div>
