<style type="text/css">
	body{
		background-color: #f1f1f1;
	}
</style>
<div class="container-dashboard">
  <div class="head-menu-kasir">
  	<div class="kepala_keranjang">
    	<h4 align="left"><a href="<?=base_url()?>kasir"> &nbsp;<i class="fa fa-chevron-left"></i> &nbsp; Dashboard</a></h4>
	</div>
  </div>
  	<div class="content-menu-kasir">
    	<h2 align="center">Pesanan Selesai</h2>
    	<p align="center" class="sub-title">Daftar pesanan yang sedang di proses oleh Dapur</p>
    <?php
        foreach ($pesanan as $pesanan) {
        ?>
        <div class="list-bar-kasir" onClick="document.location.href='<?=base_url()?>kasir/detail_list/<?=$pesanan->id_pesanan?>/<?=$pesanan->status?>'">
          <h3 class="title-list-bar-kasir"><?=$pesanan->nm_pemesan?> <span style="float: right;"><?=$pesanan->kd_pesanan?></span></h3>
          <ul class="main-menu-bar-dapur">
            <li>
              <span>Quantity</span><br>
              <b><?=$pesanan->jml_pesan?></b>
            </li>
            <li>
              <span>No Meja</span><br>
              <b><?=$pesanan->kd_meja?></b>
            </li>
            <li>
              <span>Minute Ago</span><br>
              <b><?=$pesanan->menit?></b>
            </li>
            <li>
              <span>Status</span><br>
              <b><?=$pesanan->status_bayar?></b>
            </li>
          </ul>
          
        </div>
        <?php
        }
        ?>
    