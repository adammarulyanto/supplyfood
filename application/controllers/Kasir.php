<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kasir extends CI_Controller {
	function __construct(){
		parent::__construct();

			if($this->session->userdata('status') != "login" || $this->session->userdata('level') != "kasir"){
			redirect(base_url().'login?alert=belum_login');
		}
	}

	public function index(){
		$data['portofolio'] = $this->db->query("SELECT
	count( DISTINCT ( CASE WHEN dp.STATUS = '0' THEN dp.id_pesanan END ) ) waiting,
	count( DISTINCT ( CASE WHEN dp.STATUS IN ('1' , '2', '3' ) THEN dp.id_pesanan END ) ) dapur,
	count( DISTINCT ( CASE WHEN dp.STATUS = '4' THEN dp.id_pesanan END ) ) done,
	count( DISTINCT ( CASE WHEN pb.id_pembayaran IS NOT NULL and pb.status=1 THEN dp.id_pesanan END ) ) pembayaran 
FROM
	detail_pesanan dp
	LEFT JOIN pembayaran pb ON pb.id_pesanan = dp.id_pesanan")->row();
		$this->load->view('header');
		$this->load->view('kasir/home',$data);
		$this->load->view('footer');
	}

	function waiting_list(){
		$data['pesanan'] = $this->db->query("
			SELECT dp.status,dp.id_pesanan,p.kd_pesanan,p.nm_pemesan,p.status,m.kd_meja,round(count(case when dp.status = 3 then dp.id_detail_pesanan end)/count(*)*100,1) persen_selesai, format(sum(harga*qty),0) total_harga,sum(qty) jml_pesan,timestampdiff(minute,p.tgl_pesan,current_timestamp) menit
			from 
			detail_pesanan dp 
			inner join pesanan p on dp.id_pesanan = p.id_pesanan 
			inner join produk pd on pd.id_produk = dp.id_produk
			inner join meja m on m.id_meja = p.id_meja
			WHERE
			dp.status='0'
			GROUP BY
			dp.id_pesanan
			ORDER BY
			dp.id_pesanan DESC")->result();
		$this->load->view('header');
		$this->load->view('kasir/waiting',$data);
		$this->load->view('footer');
	}

	function detail_list(){
		$id_pesanan = $this->uri->segment(3);
		$id_status = $this->uri->segment(4);
		$data['det_pesanan'] = $this->db->query("
			SELECT dp.id_pesanan,dp.id_detail_pesanan,dp.status,dp.id_produk,nm_pemesan,nm_produk,qty,harga,format(harga*qty,0) total_harga,desc_produk 
			from 
			detail_pesanan dp 
			inner join pesanan p on dp.id_pesanan = p.id_pesanan 
			inner join produk pd on pd.id_produk = dp.id_produk 
			where dp.id_pesanan = '$id_pesanan'")->result();
		$data['det'] = $this->db->query("SELECT * from detail_pesanan d inner join pesanan p on d.id_pesanan = p.id_pesanan inner join meja m on m.id_meja = p.id_meja where d.id_pesanan = $id_pesanan")->row();
		$this->load->view('header');
		$this->load->view('kasir/det_list',$data);
		$this->load->view('footer');
	}

	function push_all(){
		$id_pesanan = $this->uri->segment(3);
		$this->db->query("UPDATE detail_pesanan set status=status+1 where id_pesanan = $id_pesanan");
		redirect(base_url().'kasir/waiting_list');
	}



	function dapur_list(){
		$data['pesanan'] = $this->db->query("
			SELECT dpu.nm_user,dp.id_pesanan,p.kd_pesanan,p.nm_pemesan,dp.status,m.kd_meja,round(sum(case when dp.status = '4' then dp.qty end)/sum(qty)*100,1) persen_selesai, format(sum(harga*qty),0) total_harga,sum(qty) jml_pesan,timestampdiff(minute,p.tgl_pesan,current_timestamp) menit
			from 
			detail_pesanan dp 
			inner join pesanan p on dp.id_pesanan = p.id_pesanan 
			inner join produk pd on pd.id_produk = dp.id_produk
			inner join meja m on m.id_meja = p.id_meja
			left join user pu on p.id_user = pu.id_user
			left join detail_user dpu on dpu.id_user = pu.id_user
			WHERE dp.status in ('1','2','3')
			GROUP BY
			dp.id_pesanan")->result();
		$this->load->view('header');
		$this->load->view('kasir/dapur_list',$data);
		$this->load->view('footer');
	}

		function done_list(){
		$data['pesanan'] = $this->db->query("
			SELECT dpu.nm_user,dp.id_pesanan,p.kd_pesanan,p.nm_pemesan,dp.status,m.kd_meja,round(sum(case when dp.status = '4' then dp.qty end)/sum(qty)*100,1) persen_selesai, format(sum(harga*qty),0) total_harga,sum(qty) jml_pesan,timestampdiff(minute,p.tgl_pesan,current_timestamp) menit,case when id_pembayaran is null then 'Belum Bayar' when pb.status = 1 then 'Request Bayar' when pb.status = 2 then 'Lunas' end status_bayar
			from 
			detail_pesanan dp 
			inner join pesanan p on dp.id_pesanan = p.id_pesanan 
			inner join produk pd on pd.id_produk = dp.id_produk
			inner join meja m on m.id_meja = p.id_meja
			left join user pu on p.id_user = pu.id_user
			left join detail_user dpu on dpu.id_user = pu.id_user
			left join pembayaran pb on pb.id_pesanan = p.id_pesanan
			GROUP BY
			dp.id_pesanan
			HAVING
			sum(case when dp.status = '4' then dp.qty end)/sum(qty)*100 = '100'")->result();
		$this->load->view('header');
		$this->load->view('kasir/done_list',$data);
		$this->load->view('footer');
	}

	function update_status(){
		$id_dpesan = $this->uri->segment(3);

		$this->db->query("UPDATE detail_pesanan set status=status+1 where id_detail_pesanan = $id_dpesan");
		redirect(base_url().'kasir/waiting_list');
	}

	function pembayaran(){
		if(isset($_GET['kd_pesanan'])){
			$kd_pesanan = $_GET['kd_pesanan'];
			if($kd_pesanan==''){
				$data['list_bayar'] = $this->db->query("select p.id_pesanan,kd_pesanan,total_tagihan,nominal_bayar,kembali,nm_pemesan,sum(qty) qty,kd_pembayaran from pembayaran pb inner join pesanan p on p.id_pesanan = pb.id_pesanan inner join detail_pesanan dp on dp.id_pesanan = p.id_pesanan WHERE p.status=1 and pb.status=1 GROUP BY pb.kd_pembayaran")->result();
			}else{
				$data['list_bayar'] = $this->db->query("select p.id_pesanan,kd_pesanan,sum(total_tagihan) total_tagihan,kd_pembayaran,sum(nominal_bayar) nominal_bayar,sum(kembali) kembali,nm_pemesan,sum(qty) qty from pembayaran pb inner join pesanan p on p.id_pesanan = pb.id_pesanan inner join detail_pesanan dp on dp.id_pesanan = p.id_pesanan WHERE pb.kd_pembayaran = dp.kd_pembayaran and pb.status=1 GROUP BY pb.kd_pembayaran")->result();
			}
		}else{
		$data['list_bayar'] = $this->db->query("select p.id_pesanan,pb.kd_pembayaran,kd_pesanan,sum(total_tagihan) total_tagihan,sum(nominal_bayar) nominal_bayar,sum(kembali) kembali,nm_pemesan,sum(qty) qty from pembayaran pb inner join pesanan p on p.id_pesanan = pb.id_pesanan inner join detail_pesanan dp on dp.id_pesanan = p.id_pesanan WHERE pb.status=1 GROUP BY pb.kd_pembayaran")->result();
		}
		$this->load->view('header');
		$this->load->view('kasir/pembayaran',$data);
		$this->load->view('footer');
	}

	function det_pembayaran(){
		$id_pesan = $this->uri->segment(3);
		$data['bayar'] = $this->db->query("SELECT p.id_pesanan,dp.kd_pembayaran,kd_pesanan,kembali,nominal_bayar,nm_pemesan,format(sum(harga*qty),0) harga,format(sum(harga*qty),0) total_bayar from detail_pesanan dp inner join pesanan p on p.id_pesanan = dp.id_pesanan inner join pembayaran pb on pb.kd_pembayaran = dp.kd_pembayaran inner join produk pr on pr.id_produk = dp.id_produk where pb.kd_pembayaran = $id_pesan")->row();
		$data['list_bayar'] = $this->db->query("SELECT
													nm_produk,
													sum(qty) qty,
													format(sum(harga*qty),0) harga 
												FROM
													detail_pesanan dp
													INNER JOIN pesanan p ON p.id_pesanan = dp.id_pesanan
													left join pembayaran pb on pb.kd_pembayaran = dp.kd_pembayaran
													inner join produk pr on pr.id_produk = dp.id_produk
												WHERE
													dp.kd_pembayaran = $id_pesan
												GROUP BY
													dp.id_produk")->result();
		$this->load->view('header');
		$this->load->view('kasir/det_pembayaran',$data);
		$this->load->view('footer');
	}

	function pesanan_lunas(){
		$id_pembayaran = $this->uri->segment(3);
		$update_pesanan = $this->db->query("UPDATE detail_pesanan p set status_pembayaran= 2 where p.kd_pembayaran = '$id_pembayaran'");
		$update_pembayaran = $this->db->query("UPDATE pembayaran pb set status = '2',tgl_bayar=now() where pb.kd_pembayaran = '$id_pembayaran'");
		redirect(base_url().'kasir/pembayaran?alert=berhasil');
	}

}
?>