<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dapur extends CI_Controller {
	function __construct(){
		parent::__construct();

			if($this->session->userdata('status') != "login" || $this->session->userdata('level') != "dapur"){
			redirect(base_url().'login?alert=belum_login');
			}
	}


	function index()
	{		
		$id_user = $this->session->userdata('id');
		$data['jam'] = $this->db->query("select date_format(CURRENT_TIMESTAMP,'%h:%i') jam")->row();
		$data['waiting'] = $this->db->query("SELECT persen_selesai,p.id_pesanan,kd_meja,TIMESTAMPDIFF(minute,tgl_pesan,CURRENT_TIMESTAMP) minute,sum(qty) qty from pesanan p inner join detail_pesanan dp on dp.id_pesanan = p.id_pesanan inner join meja m on p.id_meja = m.id_meja left join (SELECT
				dp.id_pesanan id_pesan,sum( CASE WHEN dp.STATUS = '4' THEN dp.qty END ) / sum( qty ) * 100 persen_selesai 
			FROM
				detail_pesanan dp 
			GROUP BY
				dp.id_pesanan) summary on id_pesan = p.id_pesanan where dp.status='1' and p.status='1' group by p.id_pesanan")->result();


		$data['cooking'] = $this->db->query("SELECT persen_selesai,p.id_pesanan,kd_meja,TIMESTAMPDIFF(minute,tgl_pesan,CURRENT_TIMESTAMP) minute,sum(qty) qty from pesanan p inner join detail_pesanan dp on dp.id_pesanan = p.id_pesanan inner join meja m on p.id_meja = m.id_meja left join (SELECT
				dp.id_pesanan id_pesan,sum( CASE WHEN dp.STATUS = '4' THEN dp.qty END ) / sum( qty ) * 100 persen_selesai 
			FROM
				detail_pesanan dp 
			GROUP BY
				dp.id_pesanan) summary on id_pesan = p.id_pesanan where dp.status='2' and p.status='1' and dp.id_user = $id_user group by p.id_pesanan")->result();


		$data['finishing'] = $this->db->query("SELECT persen_selesai,p.id_pesanan,kd_meja,TIMESTAMPDIFF(minute,tgl_pesan,CURRENT_TIMESTAMP) minute,sum(qty) qty from pesanan p inner join detail_pesanan dp on dp.id_pesanan = p.id_pesanan inner join meja m on p.id_meja = m.id_meja left join (SELECT
				dp.id_pesanan id_pesan,sum( CASE WHEN dp.STATUS = '4' THEN dp.qty END ) / sum( qty ) * 100 persen_selesai 
			FROM
				detail_pesanan dp 
			GROUP BY
				dp.id_pesanan) summary on id_pesan = p.id_pesanan where dp.status='3' and p.status='1' and dp.id_user = $id_user group by p.id_pesanan")->result();


		$data['done'] = $this->db->query("SELECT persen_selesai,p.id_pesanan,kd_meja,TIMESTAMPDIFF(minute,tgl_pesan,CURRENT_TIMESTAMP) minute,sum(qty) qty from pesanan p inner join detail_pesanan dp on dp.id_pesanan = p.id_pesanan inner join meja m on p.id_meja = m.id_meja left join (SELECT
				dp.id_pesanan id_pesan,sum( CASE WHEN dp.STATUS = '4' THEN dp.qty END ) / sum( qty ) * 100 persen_selesai 
			FROM
				detail_pesanan dp 
			GROUP BY
				dp.id_pesanan) summary on id_pesan = p.id_pesanan where dp.status='4' and p.status='1' group by p.id_pesanan")->result();
		$this->load->view('header');
		$this->load->view('dapur/home',$data);
		$this->load->view('footer');
	}

	function modal(){
		$this->load->view('header');
		$this->load->view('footer');
	}


	function det_list_dapur(){
		$id_pesanan = $this->uri->segment(3);
		$id_status = $this->uri->segment(4);
		$data['det_pesanan'] = $this->db->query("
			SELECT dp.id_detail_pesanan,dp.status,dp.id_produk,nm_pemesan,nm_produk,qty,harga,format(harga*qty,0) total_harga,desc_produk 
			from 
			detail_pesanan dp 
			inner join pesanan p on dp.id_pesanan = p.id_pesanan 
			inner join produk pd on pd.id_produk = dp.id_produk 
			where dp.id_pesanan = '$id_pesanan' and dp.status='$id_status'")->result();
		$data['det'] = $this->db->query("SELECT * from detail_pesanan d inner join pesanan p on d.id_pesanan = p.id_pesanan inner join meja m on m.id_meja = p.id_meja where d.id_pesanan = $id_pesanan and d.status='$id_status'")->row();
		$this->load->view('header');
		$this->load->view('dapur/det_list_dapur',$data);
		$this->load->view('footer');
	}

	function update_status(){
		$id_dpesan = $this->uri->segment(3);
		$id_user = $this->session->userdata('id');

		$this->db->query("UPDATE detail_pesanan set status=status+1 , id_user=$id_user where id_detail_pesanan = $id_dpesan");
		redirect(base_url().'dapur');
	}

}
