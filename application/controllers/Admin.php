<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {
	function __construct(){
		parent::__construct();

			if($this->session->userdata('status') != "login" || $this->session->userdata('level') != "admin"){
			redirect(base_url().'login?alert=belum_login');
		}
	}


	function index()
	{		
		$data['data'] = $this->db->query("select count(*) user,persediaan,meja,pesanan from `user` join (select count(*) persediaan from produk) produk join (select count(*) meja from meja) meja join (select count(*) pesanan from pesanan where status=2) pesanan")->row();
		$this->load->view('admin/sidemenu');
		$this->load->view('header');
		$this->load->view('admin/home',$data);
		$this->load->view('footer');
	}

	function anggota()
	{		
		$data['anggota'] = $this->db->query("SELECT if(u.status='N','Inactive','Active') status_user,u.id_user,du.nm_user,ul.nm_level,ifnull(du.email,'No Email') email,u.username from user u inner join user_level ul on u.id_user_level = ul.id_user_level inner join detail_user du on du.id_user = u.id_user ORDER BY u.id_user DESC")->result();
		$this->load->view('admin/sidemenu');
		$this->load->view('header');
		$this->load->view('admin/berhasil');
		$this->load->view('admin/anggota',$data);
		$this->load->view('footer');
	}

	function transaksi()
	{		
		$data['pesanan'] = $this->db->query("select *,case status when 1 then 'Active' when 2 then 'Selesai' when 0 then 'Cancel' end status_pesanan from pesanan ORDER by tgl_pesan desc")->result();
		$data['detail_pesanan'] = $this->db->query("select * from detail_pesanan")->result();
		$data['pembayaran'] = $this->db->query("select *,ps.status status2,case pb.status when 1 then 'Request' when 2 then 'Selesai' end status_pembayaran,case ps.status when 1 then 'Active' when 2 then 'Selesai' when 0 then 'Cancel' end status_pesanan from pembayaran pb inner join pesanan ps on ps.id_pesanan = pb.id_pesanan group by pb.kd_pembayaran")->result();
		$this->load->view('admin/sidemenu');
		$this->load->view('header');
		$this->load->view('admin/berhasil');
		$this->load->view('admin/transaksi',$data);
		$this->load->view('footer');
	}

	function detail_pesanan()
	{		
		$id = $id = $this->uri->segment('3');
		$data['detail_pesanan'] = $this->db->query("SELECT kd_produk,nm_produk,harga,dp.qty,nm_user,CASE dp.STATUS WHEN '0' THEN 'Keranjang' WHEN '1' THEN 'Waiting List' WHEN '2' THEN 'Cooking' WHEN '3' THEN 'Finishing' WHEN '4' THEN 'Selesai' END status2 FROM detail_pesanan dp INNER JOIN produk pd ON pd.id_produk=dp.id_produk INNER JOIN USER u ON u.id_user=dp.id_user INNER JOIN detail_user du ON du.id_user=u.id_user INNER JOIN pesanan p ON p.id_pesanan=dp.id_pesanan WHERE dp.id_pesanan=$id")->result();
		$data['pesanan'] = $this->db->query("select * from pesanan where id_pesanan = $id")->row();
		$this->load->view('admin/sidemenu');
		$this->load->view('header');
		$this->load->view('admin/berhasil');
		$this->load->view('admin/detail_pesanan',$data);
		$this->load->view('footer');
	}

	function detail_pembayaran()
	{		
		$id = $id = $this->uri->segment('3');
		$data['pembayaran'] = $this->db->query("select * from pembayaran pm inner join pesanan p on p.id_pesanan = pm.id_pesanan where pm.kd_pembayaran=$id")->row();
		$data['detail_pesanan'] = $this->db->query("SELECT if(dp.status_pembayaran='0','Menunggu Pembayaran','Terbayar') status2,dp.status_pembayaran,kd_produk,nm_produk,harga*dp.qty harga,dp.qty,nm_user FROM detail_pesanan dp INNER JOIN produk pd ON pd.id_produk=dp.id_produk INNER JOIN USER u ON u.id_user=dp.id_user INNER JOIN detail_user du ON du.id_user=u.id_user INNER JOIN pesanan p ON p.id_pesanan=dp.id_pesanan WHERE dp.kd_pembayaran=$id")->result();
		$data['pesanan'] = $this->db->query("select * from pesanan where id_pesanan = $id")->row();
		$this->load->view('admin/sidemenu');
		$this->load->view('header');
		$this->load->view('admin/berhasil');
		$this->load->view('admin/detail_pembayaran',$data);
		$this->load->view('footer');
	}

	function cetak_detail_pesanan(){
		$id = $id = $this->uri->segment('3');
		$data['detail_pesanan'] = $this->db->query("SELECT kd_produk,nm_produk,harga,dp.qty,nm_user,CASE dp.STATUS WHEN '0' THEN 'Keranjang' WHEN '1' THEN 'Waiting List' WHEN '2' THEN 'Cooking' WHEN '3' THEN 'Finishing' WHEN '4' THEN 'Selesai' END status2 FROM detail_pesanan dp INNER JOIN produk pd ON pd.id_produk=dp.id_produk INNER JOIN USER u ON u.id_user=dp.id_user INNER JOIN detail_user du ON du.id_user=u.id_user INNER JOIN pesanan p ON p.id_pesanan=dp.id_pesanan WHERE dp.id_pesanan=$id")->result();
		$data['pesanan'] = $this->db->query("select * from pesanan where id_pesanan = $id")->row();
		$this->load->view('header');
		$this->load->view('admin/berhasil');
		$this->load->view('admin/cetak_detail_pesanan',$data);
		$this->load->view('footer');
	}

	function cetak_detail_pembayaran(){
		$id = $id = $this->uri->segment('3');
		$data['pembayaran'] = $this->db->query("select pm.kd_pembayaran,nm_pemesan,kd_pesanan,tgl_bayar,pm.id_pesanan,format(sum(total_tagihan),0) total_tagihan,format(sum(nominal_bayar),0) nominal_bayar,format(sum(kembali),0) kembali from pembayaran pm inner join pesanan p on p.id_pesanan = pm.id_pesanan where pm.kd_pembayaran=$id")->row();
		$data['detail_pesanan'] = $this->db->query("SELECT if(dp.status_pembayaran='0','Menunggu Pembayaran','Terbayar') status2,dp.status_pembayaran,kd_produk,nm_produk,harga*dp.qty harga,dp.qty,nm_user FROM detail_pesanan dp INNER JOIN produk pd ON pd.id_produk=dp.id_produk INNER JOIN USER u ON u.id_user=dp.id_user INNER JOIN detail_user du ON du.id_user=u.id_user INNER JOIN pesanan p ON p.id_pesanan=dp.id_pesanan WHERE dp.kd_pembayaran=$id")->result();
		$data['pesanan'] = $this->db->query("select * from pesanan where id_pesanan = $id")->row();
		$this->load->view('header');
		$this->load->view('admin/berhasil');
		$this->load->view('admin/cetak_detail_pembayaran',$data);
		$this->load->view('footer');
	}

	function cetak_pesanan(){
		$data['pesanan'] = $this->db->query("select p.status,case when p.status='1' then 'Active' when p.status ='2' then 'Selesai' when p.status='0' then 'Cancel' end status2,kd_pesanan,nm_pemesan,kd_meja,tgl_pesan,nm_user from pesanan p inner join user u on u.id_user = p.id_user inner join meja m on m.id_meja = p.id_meja inner join detail_user du on du.id_user = u.id_user ORDER by tgl_pesan desc")->result();
		$this->load->view('header');
		$this->load->view('admin/berhasil');
		$this->load->view('admin/cetak_pesanan',$data);
		$this->load->view('footer');
	}


	function cetak_pembayaran(){
		$data['pembayaran'] = $this->db->query("select *,pb.status status2,case pb.status when 1 then 'Request' when 2 then 'Selesai' end status_pembayaran from pembayaran pb inner join pesanan ps on ps.id_pesanan = pb.id_pesanan")->result();
		$this->load->view('header');
		$this->load->view('admin/berhasil');
		$this->load->view('admin/cetak_pembayaran',$data);
		$this->load->view('footer');
	}

	function persediaan()
	{		
		$data['persediaan'] = $this->db->query("SELECT *,if(p.status='N','Inactive','Active') status_persediaan from produk p inner join jenis_produk jp on p.id_jenis_produk = jp.id_jenis_produk")->result();
		$this->load->view('admin/sidemenu');
		$this->load->view('header');
		$this->load->view('admin/berhasil');
		$this->load->view('admin/persediaan',$data);
		$this->load->view('footer');
	}

	function meja()
	{		
		$data['meja'] = $this->db->query("SELECT id_meja,m.kd_meja,du.nm_user,if(m.status='N','Inactive','Active') status_meja from meja m left join user u on m.id_user = u.id_user left join detail_user du on du.id_user = u.id_user ")->result();
		$this->load->view('admin/sidemenu');
		$this->load->view('header');
		$this->load->view('admin/berhasil');
		$this->load->view('admin/meja',$data);
		$this->load->view('footer');
	}


	function add_anggota()
	{		
		$data['level'] = $this->db->query("SELECT * FROM user_level")->result();
		$this->load->view('admin/sidemenu');
		$this->load->view('header');
		$this->load->view('admin/add_anggota',$data);
		$this->load->view('footer');
	}

	function edit_anggota()
	{		
		$id = $this->uri->segment('3');
		$data['anggota'] = $this->db->query("SELECT u.id_user,u.username,du.nm_user,du.telp,du.email from user u inner join user_level ul on u.id_user_level = ul.id_user_level inner join detail_user du on du.id_user = u.id_user WHERE u.id_user=$id")->row();
		$data['level'] = $this->db->query("SELECT * FROM user_level")->result();
		$this->load->view('admin/sidemenu');
		$this->load->view('header');
		$this->load->view('admin/edit_anggota',$data);
		$this->load->view('footer');
	}

	function add_persediaan()
	{		
		$data['jproduk'] = $this->db->query("SELECT * FROM jenis_produk")->result();
		$this->load->view('admin/sidemenu');
		$this->load->view('header');
		$this->load->view('admin/add_persediaan',$data);
		$this->load->view('footer');
	}


	function edit_persediaan()
	{		
		$id = $this->uri->segment('3');
		$data['persediaan'] = $this->db->query("SELECT * from produk p inner join jenis_produk jp on p.id_jenis_produk = jp.id_jenis_produk where id_produk=$id")->row();
		$data['jproduk'] = $this->db->query("SELECT * FROM jenis_produk")->result();
		$this->load->view('admin/sidemenu');
		$this->load->view('header');
		$this->load->view('admin/edit_persediaan',$data);
		$this->load->view('footer');
	}

	function edit_meja()
	{		
		$id = $this->uri->segment('3');
		$data ['meja'] = $this->db->query("SELECT * from meja where id_meja = $id")->row();
		$data['usermeja'] = $this->db->query("SELECT u.id_user,du.nm_user FROM user u inner join detail_user du on du.id_user = u.id_user where u.id_user not in (select id_user from meja) and u.id_user_level = 4")->result();
		$this->load->view('admin/sidemenu');
		$this->load->view('header');
		$this->load->view('admin/edit_meja',$data);
		$this->load->view('footer');
	}

	function add_meja()
	{		
		$data['usermeja'] = $this->db->query("SELECT u.id_user,du.nm_user FROM user u inner join detail_user du on du.id_user = u.id_user where u.id_user not in (select id_user from meja) and u.id_user_level = 4")->result();
		$this->load->view('admin/sidemenu');
		$this->load->view('header');
		$this->load->view('admin/add_meja',$data);
		$this->load->view('footer');
	}

	function add_meja_act(){
		$kd_meja = $this->input->post('kd_meja');
		$usermeja = $this->input->post('usermeja');

		$insert_user = $this->db->query("INSERT INTO meja values ('','$usermeja','$kd_meja','N')");
		redirect(base_url().'admin/meja?warn=berhasil');
	}

	function add_anggota_act(){
		$username = $this->input->post('username');
		$password = sha1($this->input->post('password'));
		$nm_user = $this->input->post('nm_user');
		$telp = $this->input->post('telp');
		$email = $this->input->post('email');
		$level = $this->input->post('level');

		$insert_user = $this->db->query("INSERT INTO user values ('','$level','$username','$password','Y')");
		$id_user = $this->db->query("SELECT id_user from user where username='$username' and password = '$password'")->row();
		$insert_det_user = $this->db->query("INSERT INTO detail_user values ('','$id_user->id_user','$nm_user','$telp','$email')");
		redirect(base_url().'admin/anggota?warn=berhasil');
	}

	function add_persediaan_act(){
		$nm_produk = $this->input->post('nm_produk');
		$desc_produk = $this->input->post('desc_produk');
		$harga = $this->input->post('harga');
		$jproduk = $this->input->post('jproduk');
		$kd_jproduk = $this->db->query("SELECT * from jenis_produk where id_jenis_produk = $jproduk")->row();
		$gen_data = $this->db->query("select concat('$kd_jproduk->kd_jenis_produk',LPAD(count(*)+1,8,0)) kode from produk where id_jenis_produk = $jproduk ")->row();
		$gambar = $_FILES["gambar"]["name"];
		$temp = explode(".", $_FILES["gambar"]["name"]);
		$newfilename = round(microtime(true)) . '.' . end($temp);
		 if (move_uploaded_file($_FILES["gambar"]["tmp_name"], "assets/images/produk/makanan/". $newfilename)) {
        	echo "The file ". $newfilename. " has been uploaded.";
	    } else {
	    	echo "The file ". $newfilename. " has been uploaded.";
	        // redirect(base_url().'admin/add_persediaan?upload=gagal');
	    }

		$insert_produk = $this->db->query("INSERT INTO produk values ('','$jproduk','$gen_data->kode','$nm_produk','$desc_produk','$newfilename','$harga','Y')");
		redirect(base_url().'admin/persediaan?warn=berhasil');
	}

	function edit_meja_act(){
		$id = $this->input->post('id');
		$old_user = $this->db->query("SELECT id_user from meja WHERE id_meja=$id")->row();
		$username = $this->input->post('username');
		if($this->input->post('usermeja')==''){
			$user = $old_user->id_user;
		}else{
			$user = $this->input->post('usermeja');
		}
		$kd_meja = $this->input->post('kd_meja');

		$update_user = $this->db->query("UPDATE meja set id_user='$user', kd_meja='$kd_meja' where id_meja = '$id'");
		redirect(base_url().'admin/meja?warn=berhasil');
	}

	function edit_anggota_act(){
		$id = $this->input->post('id');
		$old_password = $this->db->query("SELECT password from user WHERE id_user=$id")->row();
		$username = $this->input->post('username');
		if($this->input->post('password')==''){
			$password = $old_password->password;
		}else{
			$password = sha1($this->input->post('password'));
		}
		$nm_user = $this->input->post('nm_user');
		$telp = $this->input->post('telp');
		$email = $this->input->post('email');
		$level = $this->input->post('level');

		$update_user = $this->db->query("UPDATE user set username='$username',password='$password',id_user_level='$level' where id_user = $id");
		$update_det_user = $this->db->query("UPDATE detail_user SET nm_user='$nm_user',telp='$telp',email='$email' where id_user=$id");
		redirect(base_url().'admin/anggota?warn=berhasil');
	}

	function edit_persediaan_act(){
		$id_produk = $this->input->post('id_produk');
		$kd_produk = $this->input->post('kd_produk');
		$nm_produk = $this->input->post('nm_produk');
		$desc_produk = $this->input->post('desc_produk');
		$harga = $this->input->post('harga');
		$jproduk = $this->input->post('jproduk');
		$target_file = $target_dir . basename($_FILES["gambar"]["name"]);
		$gambar = $_FILES["gambar"]["name"];
			$temp = explode(".", $_FILES["gambar"]["name"]);
			$newfilename = round(microtime(true)) . '.' . end($temp);
			if (move_uploaded_file($_FILES["gambar"]["tmp_name"], "assets/images/produk/makanan/". $newfilename)) {
	        	echo "The file ". $newfilename. " has been uploaded.";
		    } else {
		        $newfilename = $this->input->post('old_gambar');
		    }

		$update = $this->db->query("UPDATE produk set nm_produk='$nm_produk',desc_produk='$desc_produk',harga='$harga',gambar='$newfilename',id_jenis_produk='$jproduk' where id_produk = $id_produk");
		redirect(base_url().'admin/persediaan?warn=berhasil');
	}

	function inactive_anggota(){
		$id = $this->uri->segment('3');
		$del_det_user = $this->db->query("UPDATE user set status = if(status='N','Y','N') where id_user=$id");
		redirect(base_url().'admin/anggota?warn=berhasil');
	}

	function inactive_meja(){
		$id = $this->uri->segment('3');
		$del_det_meja = $this->db->query("UPDATE meja set status = if(status='N','Y','N') where id_meja=$id");
		redirect(base_url().'admin/meja?warn=berhasil');
	}

	function inactive_persediaan(){
		$id = $this->uri->segment('3');
		$del_det_meja = $this->db->query("UPDATE produk set status = if(status='N','Y','N') where id_produk=$id");
		redirect(base_url().'admin/persediaan?warn=berhasil');
	}
}