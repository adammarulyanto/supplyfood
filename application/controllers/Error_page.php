<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Error_page extends CI_Controller {
	function __construct(){
		parent::__construct();
	}

	function index()
	{		
		$this->load->view('header');
		$this->load->view('404');
		$this->load->view('footer');
	}
}
