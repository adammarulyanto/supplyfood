<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Meja extends CI_Controller {
	function __construct(){
		parent::__construct();

			if($this->session->userdata('status') != "login" || $this->session->userdata('level') != "meja"){
			redirect(base_url().'login?alert=belum_login');
		}
			// $id_user = $this->session->userdata('id');
			// $query = $this->db->query("SELECT * from pesanan p inner join pembayaran pb on pb.id_pesanan = p.id_pesanan  where id_user = $id_user and p.status='1'");
			// $data = $query->row();
			// $row = $query->num_rows();
			// if($row>0){
			//     redirect(base_url().'welcome/trims/'.$data->id_pesanan);
			// }
	}

	function index()
	{		
		$id_meja = $this->session->userdata('idmeja');
		$query = $this->db->query("SELECT * from meja where id_meja=$id_meja");
		$data['idmeja'] = $query->row();
		$this->load->view('header');
		$this->load->view('meja/home',$data);
		$this->load->view('footer');
	}

	function menu(){
		$id_meja = $this->session->userdata('idmeja');
		$data['produk'] = $this->db->query("select * from produk where status='Y'")->result();
		$kd_pesanan = $_GET['kd_pesanan'];
		$query1 = $this->db->query("select id_pesanan,nm_pemesan,ifnull((select sum(qty) from detail_pesanan dp where dp.id_pesanan = p.id_pesanan),0) jml from pesanan p where kd_pesanan='$kd_pesanan'");
		$data['pesanan'] = $query1->row();
		$query_meja = $this->db->query("SELECT * from meja where id_meja=$id_meja");
		$data['idmeja'] = $query_meja->row();
		$this->load->view('header');
		$this->load->view('meja/menu',$data);
		$this->load->view('sec_footer_menu');
		$this->load->view('footer');
	}

	function pesan1_act(){
		$pemesan = $this->input->post('nama_pemesan');
		$query = $this->db->query("select concat(date_format(curdate(),'%y%m%d'),lpad(count(*)+1,2,0)) kode from pesanan");
		$kode = $query->row();
		$id_meja = $this->session->userdata('idmeja');
		$iduser = $this->session->userdata('id');
		$data = array(
			'kd_pesanan'	=> $kode->kode,
			'nm_pemesan' 	=> $pemesan,
			'id_meja'		=> $id_meja,
			'id_user'		=> $iduser
		);

		$this->main_model->insert_data($data,'pesanan');
		redirect(base_url().'meja/menu/1?kd_pesanan='.$kode->kode);
	}

	function tambah_keranjang($id){
		$query = $this->db->query("SELECT id_pesanan from pesanan where kd_pesanan='$_GET[kd_pesanan]'");
		$id_pesanan = $query->row();
		$query2 = $this->db->query("SELECT ifnull(sum(qty),0) quantity from detail_pesanan where id_pesanan=$id_pesanan->id_pesanan and id_produk=$id and status='0' and status_pembayaran='0' and kd_pembayaran is null");
		$d = $query2->row();
		$isi = array(
			'id_pesanan' 	=> $id_pesanan->id_pesanan,
			'id_produk' 	=> $id,
			'qty'			=> $d->quantity+1
		);
		if($d->quantity>0){
			$where = array('id_produk' => $id,'id_pesanan' => $id_pesanan->id_pesanan, 'status' => '0', 'status_pembayaran' => '0', 'kd_pembayaran' => NULL);
			$data = array('qty' => $d->quantity+1);
			$this->main_model->update_data($where,$data,'detail_pesanan');
		}else{
			$this->main_model->insert_data($isi,'detail_pesanan');
		}
		// redirect(base_url()."meja/menu/1?kd_pesanan=$_GET[kd_pesanan]");
		header('Location: ' . $_SERVER['HTTP_REFERER']);
	}

	function kurang_keranjang($id){
		$query = $this->db->query("SELECT id_pesanan from pesanan where kd_pesanan='$_GET[kd_pesanan]'");
		$id_pesanan = $query->row();
		$query2 = $this->db->query("SELECT ifnull(sum(qty),0) quantity from detail_pesanan where id_pesanan=$id_pesanan->id_pesanan and id_produk=$id and status='0' and status_pembayaran='0' and kd_pembayaran is null");
		$d = $query2->row();
		$isi = array(
			'id_pesanan' 	=> $id_pesanan->id_pesanan,
			'id_produk' 	=> $id,
			'qty'			=> $d->quantity-1
		);
		if($d->quantity>1){
			$where = array('id_produk' => $id,'id_pesanan' => $id_pesanan->id_pesanan, 'status' => '0', 'status_pembayaran' => '0','kd_pembayaran' => NULL);
			$data = array('qty' => $d->quantity-1);
			$this->main_model->update_data($where,$data,'detail_pesanan');
		}else{
			$where = array('id_produk' => $id,'id_pesanan' => $id_pesanan->id_pesanan, 'status' => '0', 'status_pembayaran' => '0', 'kd_pembayaran' => NULL);
			$this->main_model->delete_data($where,'detail_pesanan');
		}
		// redirect(base_url()."meja/menu/1?kd_pesanan=$_GET[kd_pesanan]");
		header('Location: ' . $_SERVER['HTTP_REFERER']);
	}

	function keranjang(){
		$kd_pesanan = $_GET['kd_pesanan'];
		$data['stat'] = $this->db->query("select if((count(*)-count(case when dp.status_pembayaran ='1' then id_detail_pesanan end)) = 0,'selesai','blm') stat from detail_pesanan dp left join pesanan p on p.id_pesanan = dp.id_pesanan left join pembayaran pb on pb.kd_pembayaran = dp.kd_pembayaran where p.kd_pesanan = $kd_pesanan")->row();
		$data['bayar'] = $this->db->query("SELECT ifnull(format(sum(case when dp.status_pembayaran ='0' then harga*qty end),0),0) blm_terbayar,sum(harga*qty) total_bayar,if(ifnull(format(sum(case when dp.status_pembayaran ='0' then harga*qty end),0),0)<sum(harga*qty),'blmbayar','lunas') stat from detail_pesanan dp inner join pesanan p on p.id_pesanan = dp.id_pesanan inner join produk pr on pr.id_produk = dp.id_produk where p.kd_pesanan = $kd_pesanan")->row();
		$data['det_pesanan'] = $this->db->query("
			SELECT ifnull(dp.kd_pembayaran,0) kd_pembayaran,if(dp.status_pembayaran=0 and dp.kd_pembayaran is not null,'req','done') stat,dp.status,dp.id_produk,dp.status_pembayaran,nm_produk,qty,gambar,harga,harga*qty total_harga,desc_produk 
			from 
			detail_pesanan dp 
			inner join pesanan p on dp.id_pesanan = p.id_pesanan 
			inner join produk pd on pd.id_produk = dp.id_produk 
			where kd_pesanan = '$kd_pesanan'")->result();
		$data['total_harga'] = $this->db->query("
			SELECT format(sum(harga*qty),0) total_harga,p.id_pesanan
			from 
			detail_pesanan dp 
			inner join pesanan p on dp.id_pesanan = p.id_pesanan 
			inner join produk pd on pd.id_produk = dp.id_produk 
			where kd_pesanan = '$kd_pesanan'")->row();
		$this->load->view('header');
		$this->load->view('meja/keranjang',$data);
		$this->load->view('footer');
	}

	function waiting(){
		$kd_pesanan = $_GET['kd_pesanan'];
		$data['det_pesanan'] = $this->db->query("
			SELECT dp.status,dp.id_produk,nm_produk,qty,harga,format(harga*qty,0) total_harga,desc_produk 
			from 
			detail_pesanan dp 
			inner join pesanan p on dp.id_pesanan = p.id_pesanan 
			inner join produk pd on pd.id_produk = dp.id_produk 
			where kd_pesanan = '$kd_pesanan'")->result();
		$data['summary'] = $this->db->query("
			SELECT dp.id_pesanan,sum(case when dp.status = '4' then dp.qty end)/sum(qty)*100 persen_selesai, format(sum(harga*qty),0) total_harga
			from 
			detail_pesanan dp 
			inner join pesanan p on dp.id_pesanan = p.id_pesanan 
			inner join produk pd on pd.id_produk = dp.id_produk 
			where kd_pesanan = '$kd_pesanan'")->row();
		$this->load->view('header');
		$this->load->view('waiting',$data);
		$this->load->view('footer');
	}


	function det_pembayaran(){
		$id_pesan = $this->uri->segment(3);
		$data['bayar'] = $this->db->query("SELECT p.id_pesanan,kd_pesanan,nm_pemesan,ifnull(format(sum(case when dp.status_pembayaran ='0' and dp.kd_pembayaran is null then harga*qty end),0),0) blm_terbayar, sum(case when dp.status_pembayaran ='0' and dp.kd_pembayaran is null then harga*qty end) blm_terbayar_norm,ifnull(format(sum(case when dp.kd_pembayaran is not null then harga*qty end),0),0) sdh_terbayar,sum(harga*qty) total_bayar from detail_pesanan dp inner join pesanan p on p.id_pesanan = dp.id_pesanan inner join produk pr on pr.id_produk = dp.id_produk where p.id_pesanan = $id_pesan")->row();
		$data['list_bayar'] = $this->db->query("SELECT
													nm_produk,
													sum(qty) qty,
													format(sum(harga*qty),0) harga 
												FROM
													detail_pesanan dp
													INNER JOIN pesanan p ON p.id_pesanan = dp.id_pesanan
													inner join produk pr on pr.id_produk = dp.id_produk
												WHERE
													dp.id_pesanan = $id_pesan
												GROUP BY
													dp.id_produk")->result();
		$this->load->view('header');
		$this->load->view('meja/bayar',$data);
		$this->load->view('footer');
	}
	function bayar(){
		$nominal 		= $this->input->post('nominal');
		$total_bayar 	= $this->input->post('total_bayar');
		$id_pesanan 	= $this->input->post('id_pesanan');
		$kd_pesanan 	= $this->input->post('kd_pesanan');
		$kd_pembayaran	= $this->db->query("select concat(date_format(curdate(),'%Y%m%d%h%i%s'),count(*)) kode from pembayaran")->row();
		$kode = $kd_pembayaran->kode;
		if($nominal<$total_bayar){
			redirect(base_url().'meja/det_pembayaran/'.$id_pesanan);
		}
		$kembali = $nominal-$total_bayar;
		$data = array(
			'id_pesanan' 	=> $id_pesanan,
			'kd_pembayaran' => $kode,
			'total_tagihan' => $total_bayar,
			'nominal_bayar' => $nominal,
			'kembali'		=> $kembali,
			'status'		=> '1'
		);

		$insert = $this->main_model->insert_data($data,'pembayaran');
		$update = $this->db->query("update detail_pesanan set kd_pembayaran='$kode' where status_pembayaran='0' and kd_pembayaran is null");
		// $where = array(
		// 	'id_pesanan' => $id_pesanan
		// );
		// $isi = array(
		// 	'status' => '2'
		// );
		// $update = $this->main_model->update_data($data,$isi,'pesanan');
			redirect(base_url().'meja/keranjang?kd_pesanan='.$kd_pesanan);
	}

}
