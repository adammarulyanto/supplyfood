<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{	
		$this->load->view('header');
		$this->load->view('login');
		$this->load->view('footer');
	}

	function logout(){
		$where = array('id_meja' => $this->session->userdata('idmeja'));
		$data = array('status' => "N");
		$this->main_model->update_data($where,$data,'meja');
		$this->session->sess_destroy();
		redirect(base_url().'login');
	}
	function selesai(){
		$id_pesan = $this->uri->segment(3);
		$this->db->query("UPDATE pesanan set status=1 where id_pesanan = $id_pesan");
		redirect(base_url().'welcome/trims/'.$id_pesan);
	}
	
	function trims(){
		$id_pesan = $this->uri->segment(3);
		$data['bayar'] = $this->db->query("
select 
p.id_pesanan,
kd_pesanan,
nm_pemesan,
date_format(max(pb.tgl_bayar),'%d/%m/%Y') tgl_bayar,
sum(nominal_bayar) nominal_bayar,
sum(total_tagihan) total_tagihan,
sum(kembali) kembali,
p.status
from pembayaran pb
inner join pesanan p on p.id_pesanan = pb.id_pesanan
where
p.id_pesanan=2")->row();
		$data['list_bayar'] = $this->db->query("SELECT
													nm_produk,
													sum(qty) qty,
													format(sum(harga*qty),0) harga 
												FROM
													detail_pesanan dp
													INNER JOIN pesanan p ON p.id_pesanan = dp.id_pesanan
													left join pembayaran pb on dp.kd_pembayaran = pb.kd_pembayaran
													inner join produk pr on pr.id_produk = dp.id_produk
												WHERE
													dp.id_pesanan = $id_pesan
												GROUP BY
													dp.id_produk")->result();
		$this->load->view('header');
		$this->load->view('thankyou',$data);
		$this->load->view('footer');
	}
}
