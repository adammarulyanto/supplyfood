<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {
	public function index()
	{	
		if($this->session->userdata('level')=='dapur'){
		    redirect(base_url().'dapur');
		}else if($this->session->userdata('level')=='kasir'){
		    redirect(base_url().'kasir');
		}else if($this->session->userdata('level')=='meja'){
		    redirect(base_url().'meja');
		}else if($this->session->userdata('level')=='admin'){
		    redirect(base_url().'admin');
		}
		$this->load->view('header');
		$this->load->view('warning');
		$this->load->view('login');
		$this->load->view('footer');
	}

	function login_act(){
		$username = $this->input->post('username');
		$password = $this->input->post('password');

		$query = $this->db->query("SELECT u.id_user,du.nm_user,ul.nm_level from user u inner join detail_user du on du.id_user = u.id_user inner join user_level ul on ul.id_user_level = u.id_user_level where username ='$username' and password = sha1('$password')");
		$d = $query->row();
		$cek = $query->num_rows();
		if($cek>0){
			if($d->nm_level == "meja"){
				$meja = $this->db->query("SELECT id_meja from meja m where m.id_user=$d->id_user");
				$data = $meja->row();
				$row = $meja->num_rows();
				if($row>0){
				$session = array(
					'id' 		=> $d->id_user,
					'nm'		=> $d->nm_user,
					'level'		=> $d->nm_level,
					'idmeja'	=> $data->id_meja,
					'status'	=> 'login'
				);
				$this->session->set_userdata($session);
				redirect(base_url().'meja');
				}else{
					redirect(base_url().'login?alert=pilih_meja');
				}
			}else if ($d->nm_level == "kasir"){
				$session = array(
					'id' 		=> $d->id_user,
					'nm'		=> $d->nm_user,
					'level'		=> $d->nm_level,
					'status'	=> 'login'
				);
				$this->session->set_userdata($session);
				redirect(base_url().'kasir');
			}else if($d->nm_level == "dapur"){
				$session = array(
					'id' 		=> $d->id_user,
					'nm'		=> $d->nm_user,
					'level'		=> $d->nm_level,
					'status'	=> 'login'
				);
				$this->session->set_userdata($session);
				redirect(base_url().'dapur');
			}else if($d->nm_level == "admin" || $d->nm_level == "superadmin"){
				$session = array(
					'id' 		=> $d->id_user,
					'nm'		=> $d->nm_user,
					'level'		=> $d->nm_level,
					'status'	=> 'login'
				);
				$this->session->set_userdata($session);
				redirect(base_url().'admin');
			}else{
				redirect(base_url().'login?alert=belum_login');
			}
		}else{
				redirect(base_url().'login?alert=gagal_login');
		}
	}
	function meja(){
		if(isset($_GET['stat'])){
			// $data['meja'] = $this->db->query("SELECT * from meja where status='N'");
			$data['meja'] = $this->main_model->get_data('meja')->result();
			$this->load->view('header');
			$this->load->view('pilih_meja',$data);
			$this->load->view('footer');
		}else{
			redirect(base_url().'login?alert=gagal_login');
			header('Location: ' . $_SERVER['HTTP_REFERER']);
		}
	}
}
