/*
 Navicat Premium Data Transfer

 Source Server         : local
 Source Server Type    : MySQL
 Source Server Version : 100138
 Source Host           : localhost:3306
 Source Schema         : db_supplyfood

 Target Server Type    : MySQL
 Target Server Version : 100138
 File Encoding         : 65001

 Date: 24/05/2019 01:15:18
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for detail_pesanan
-- ----------------------------
DROP TABLE IF EXISTS `detail_pesanan`;
CREATE TABLE `detail_pesanan`  (
  `id_detail_pesanan` int(3) NOT NULL AUTO_INCREMENT,
  `id_pesanan` int(3) NULL DEFAULT NULL,
  `id_user` int(3) NULL DEFAULT NULL,
  `id_produk` int(3) NULL DEFAULT NULL,
  `qty` int(4) NULL DEFAULT NULL,
  `status` enum('0','1','2','3','4') CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL DEFAULT '0' COMMENT '0 : keranjang\r\n1 : Waiting List\r\n2 : Cooking\r\n3 : Finishing\r\n4 : Done',
  PRIMARY KEY (`id_detail_pesanan`) USING BTREE,
  INDEX `id_user`(`id_user`) USING BTREE,
  INDEX `id_produk`(`id_produk`) USING BTREE,
  INDEX `id_pesanan`(`id_pesanan`) USING BTREE,
  CONSTRAINT `detail_pesanan_ibfk_2` FOREIGN KEY (`id_user`) REFERENCES `user` (`id_user`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `detail_pesanan_ibfk_4` FOREIGN KEY (`id_produk`) REFERENCES `produk` (`id_produk`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `detail_pesanan_ibfk_5` FOREIGN KEY (`id_pesanan`) REFERENCES `pesanan` (`id_pesanan`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 21 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of detail_pesanan
-- ----------------------------
INSERT INTO `detail_pesanan` VALUES (6, 1, 6, 6, 1, '4');
INSERT INTO `detail_pesanan` VALUES (7, 1, 6, 3, 2, '4');
INSERT INTO `detail_pesanan` VALUES (8, 1, 6, 2, 1, '4');
INSERT INTO `detail_pesanan` VALUES (10, 3, 21, 1, 3, '4');
INSERT INTO `detail_pesanan` VALUES (11, 3, 21, 3, 1, '4');
INSERT INTO `detail_pesanan` VALUES (12, 2, 21, 1, 3, '4');
INSERT INTO `detail_pesanan` VALUES (13, 2, 21, 3, 1, '4');
INSERT INTO `detail_pesanan` VALUES (18, 4, NULL, 1, 2, '1');
INSERT INTO `detail_pesanan` VALUES (19, 6, NULL, 3, 1, '0');
INSERT INTO `detail_pesanan` VALUES (20, 6, NULL, 6, 1, '0');

-- ----------------------------
-- Table structure for detail_user
-- ----------------------------
DROP TABLE IF EXISTS `detail_user`;
CREATE TABLE `detail_user`  (
  `id_detail_user` int(3) NOT NULL AUTO_INCREMENT,
  `id_user` int(3) NULL DEFAULT NULL,
  `nm_user` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `telp` varchar(13) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `email` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id_detail_user`) USING BTREE,
  INDEX `id_user`(`id_user`) USING BTREE,
  CONSTRAINT `detail_user_ibfk_1` FOREIGN KEY (`id_user`) REFERENCES `user` (`id_user`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 20 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of detail_user
-- ----------------------------
INSERT INTO `detail_user` VALUES (1, 1, 'Super Administator', NULL, NULL);
INSERT INTO `detail_user` VALUES (2, 2, 'Administator', NULL, NULL);
INSERT INTO `detail_user` VALUES (3, 3, 'Adam Kasir', NULL, NULL);
INSERT INTO `detail_user` VALUES (4, 4, 'Meja', NULL, NULL);
INSERT INTO `detail_user` VALUES (14, 21, 'Ika Wahyu', '08123124', 'ika@mail.com');
INSERT INTO `detail_user` VALUES (15, 22, 'Meja 1', '-', '-');
INSERT INTO `detail_user` VALUES (16, 23, 'Meja 2', '-', '-');
INSERT INTO `detail_user` VALUES (17, 24, 'Meja 3', '-', '-');
INSERT INTO `detail_user` VALUES (18, 25, 'Adam Marulyanto', '0811111111', 'adam@mail.com');
INSERT INTO `detail_user` VALUES (19, 26, 'Meja 4', '-', '-');

-- ----------------------------
-- Table structure for jenis_produk
-- ----------------------------
DROP TABLE IF EXISTS `jenis_produk`;
CREATE TABLE `jenis_produk`  (
  `id_jenis_produk` int(3) NOT NULL AUTO_INCREMENT,
  `kd_jenis_produk` varchar(2) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `nm_jenis_produk` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id_jenis_produk`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of jenis_produk
-- ----------------------------
INSERT INTO `jenis_produk` VALUES (1, 'MK', 'Makanan');
INSERT INTO `jenis_produk` VALUES (2, 'MN', 'Minuman');
INSERT INTO `jenis_produk` VALUES (3, 'DS', 'Desert');

-- ----------------------------
-- Table structure for meja
-- ----------------------------
DROP TABLE IF EXISTS `meja`;
CREATE TABLE `meja`  (
  `id_meja` int(3) NOT NULL AUTO_INCREMENT,
  `id_user` int(3) NULL DEFAULT NULL,
  `kd_meja` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `status` enum('Y','N') CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT 'N',
  PRIMARY KEY (`id_meja`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of meja
-- ----------------------------
INSERT INTO `meja` VALUES (1, 4, 'A01', 'Y');
INSERT INTO `meja` VALUES (2, 22, 'A03', 'Y');
INSERT INTO `meja` VALUES (3, 23, 'A02', 'Y');
INSERT INTO `meja` VALUES (4, 24, 'A04', 'Y');

-- ----------------------------
-- Table structure for pembayaran
-- ----------------------------
DROP TABLE IF EXISTS `pembayaran`;
CREATE TABLE `pembayaran`  (
  `id_pembayaran` int(3) NOT NULL AUTO_INCREMENT,
  `id_pesanan` int(3) NULL DEFAULT NULL,
  `total_tagihan` double(50, 2) NULL DEFAULT NULL,
  `nominal_bayar` double(50, 2) NULL DEFAULT NULL,
  `kembali` double(50, 2) NULL DEFAULT NULL,
  `status` enum('1','2') CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT '1' COMMENT '1 : request, 2 : Done',
  `tgl_bayar` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id_pembayaran`) USING BTREE,
  INDEX `id_pesanan`(`id_pesanan`) USING BTREE,
  CONSTRAINT `pembayaran_ibfk_1` FOREIGN KEY (`id_pesanan`) REFERENCES `pesanan` (`id_pesanan`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 10 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of pembayaran
-- ----------------------------
INSERT INTO `pembayaran` VALUES (6, 1, 58300.00, 60000.00, 1700.00, '2', '2019-05-22 11:41:26');
INSERT INTO `pembayaran` VALUES (8, 3, 41800.00, 60000.00, 18200.00, '2', '2019-05-22 11:41:21');
INSERT INTO `pembayaran` VALUES (9, 2, 41800.00, 60000.00, 18200.00, '1', '2019-05-22 11:41:21');

-- ----------------------------
-- Table structure for pesanan
-- ----------------------------
DROP TABLE IF EXISTS `pesanan`;
CREATE TABLE `pesanan`  (
  `id_pesanan` int(3) NOT NULL AUTO_INCREMENT,
  `kd_pesanan` varchar(12) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `id_user` int(3) NULL DEFAULT NULL,
  `nm_pemesan` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `id_meja` int(3) NULL DEFAULT NULL,
  `status` varchar(2) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT '1' COMMENT '1. Active\r\n2. Selesai\r\n0. Cancel',
  `tgl_pesan` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0),
  PRIMARY KEY (`id_pesanan`) USING BTREE,
  INDEX `id_user`(`id_user`) USING BTREE,
  INDEX `id_meja`(`id_meja`) USING BTREE,
  CONSTRAINT `pesanan_ibfk_1` FOREIGN KEY (`id_user`) REFERENCES `user` (`id_user`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `pesanan_ibfk_2` FOREIGN KEY (`id_meja`) REFERENCES `meja` (`id_meja`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of pesanan
-- ----------------------------
INSERT INTO `pesanan` VALUES (1, 'TR-190521-1', 4, 'Adam marulyanto', 1, '2', '2019-05-21 21:19:56');
INSERT INTO `pesanan` VALUES (2, 'TR-190522-2', 4, 'Adammarulyanto', 1, '2', '2019-05-22 02:32:59');
INSERT INTO `pesanan` VALUES (3, 'TR-190522-3', 22, 'Adam', 2, '2', '2019-05-22 03:13:31');
INSERT INTO `pesanan` VALUES (4, 'TR-190522-4', 4, 'Adam Marulyanto', 1, '1', '2019-05-22 22:24:31');
INSERT INTO `pesanan` VALUES (5, '19052305', 23, 'Adam', 3, '1', '2019-05-23 15:22:33');
INSERT INTO `pesanan` VALUES (6, '19052306', 22, 'Adam marulyanto', 2, '1', '2019-05-23 15:56:25');

-- ----------------------------
-- Table structure for produk
-- ----------------------------
DROP TABLE IF EXISTS `produk`;
CREATE TABLE `produk`  (
  `id_produk` int(3) NOT NULL AUTO_INCREMENT,
  `id_jenis_produk` int(3) NULL DEFAULT NULL,
  `kd_produk` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `nm_produk` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `desc_produk` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `gambar` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `harga` int(100) NULL DEFAULT NULL,
  `status` enum('Y','N') CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT 'Y',
  PRIMARY KEY (`id_produk`) USING BTREE,
  INDEX `id_produk`(`id_produk`) USING BTREE,
  INDEX `id_jenis_produk`(`id_jenis_produk`) USING BTREE,
  CONSTRAINT `produk_ibfk_1` FOREIGN KEY (`id_jenis_produk`) REFERENCES `jenis_produk` (`id_jenis_produk`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 26 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of produk
-- ----------------------------
INSERT INTO `produk` VALUES (1, 1, 'MK00000001', 'Ayam Bakar', 'Ayam yang dibakar dengan kematangan yang sangat pas', 'ayam_bakar.jpg', 20000, 'N');
INSERT INTO `produk` VALUES (2, 2, 'MN00000001', 'Jus Alpukat', 'Jus yang sangat Segar', 'jus_alpukat.jpg', 10000, 'Y');
INSERT INTO `produk` VALUES (3, 1, 'MK00000002', 'Ayam Goreng', 'Ayam Goreng yang di goreng dengan kematangan yang merata hingga ke daging', 'ayam_goreng.jpg', 18000, 'Y');
INSERT INTO `produk` VALUES (6, 1, 'MK00000003', 'Ayam Geprek', 'Ayam yang di goreng dengan minyak yang berkualitas dan sambal yang sangat lezat', 'ayam_geprek.jpg', 25000, 'Y');
INSERT INTO `produk` VALUES (25, 1, 'MK00000004', 'Telur Mata Sapi', '-', '1558494812.jpg', 5000, 'Y');

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user`  (
  `id_user` int(3) NOT NULL AUTO_INCREMENT,
  `id_user_level` int(1) NULL DEFAULT NULL,
  `username` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `password` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `status` enum('Y','N') CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT 'Y',
  PRIMARY KEY (`id_user`) USING BTREE,
  INDEX `id_user_level`(`id_user_level`) USING BTREE,
  CONSTRAINT `user_ibfk_1` FOREIGN KEY (`id_user_level`) REFERENCES `user_level` (`id_user_level`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 27 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES (1, 1, 'superadmin', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'N');
INSERT INTO `user` VALUES (2, 2, 'admin', '7c4a8d09ca3762af61e59520943dc26494f8941b', NULL);
INSERT INTO `user` VALUES (3, 3, 'kasir', '7c4a8d09ca3762af61e59520943dc26494f8941b', NULL);
INSERT INTO `user` VALUES (4, 4, 'meja', '7c4a8d09ca3762af61e59520943dc26494f8941b', NULL);
INSERT INTO `user` VALUES (6, 5, 'dapur', '7c4a8d09ca3762af61e59520943dc26494f8941b', NULL);
INSERT INTO `user` VALUES (21, 5, 'dapur', '7c4a8d09ca3762af61e59520943dc26494f8941b', NULL);
INSERT INTO `user` VALUES (22, 4, 'meja1', '7c4a8d09ca3762af61e59520943dc26494f8941b', NULL);
INSERT INTO `user` VALUES (23, 4, 'meja2', '7c4a8d09ca3762af61e59520943dc26494f8941b', NULL);
INSERT INTO `user` VALUES (24, 4, 'meja3', '7c4a8d09ca3762af61e59520943dc26494f8941b', NULL);
INSERT INTO `user` VALUES (25, 3, 'kasir', '7c4a8d09ca3762af61e59520943dc26494f8941b', NULL);
INSERT INTO `user` VALUES (26, 4, 'meja4', '7c4a8d09ca3762af61e59520943dc26494f8941b', NULL);

-- ----------------------------
-- Table structure for user_level
-- ----------------------------
DROP TABLE IF EXISTS `user_level`;
CREATE TABLE `user_level`  (
  `id_user_level` int(1) NOT NULL AUTO_INCREMENT,
  `nm_level` varchar(15) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id_user_level`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of user_level
-- ----------------------------
INSERT INTO `user_level` VALUES (1, 'superadmin');
INSERT INTO `user_level` VALUES (2, 'admin');
INSERT INTO `user_level` VALUES (3, 'kasir');
INSERT INTO `user_level` VALUES (4, 'meja');
INSERT INTO `user_level` VALUES (5, 'dapur');

SET FOREIGN_KEY_CHECKS = 1;
